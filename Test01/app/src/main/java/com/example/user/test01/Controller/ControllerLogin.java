package com.example.user.test01.Controller;

import android.content.Context;
import android.widget.Toast;

import com.example.user.test01.Model.ModelLogin;
import com.example.user.test01.Model.SQlite.SQLiteDatabaseHandler;

import java.util.ArrayList;
import java.util.List;

public class ControllerLogin {
    private Context context;
    public ControllerLogin(Context context) {
        this.context = context;
    }

    public boolean OnclickLogin(String username , String password){
        if(CheckNullText(username,password)==false) {
            Toast.makeText(context, "Vui lòng điền đủ thông tin", Toast.LENGTH_LONG).show();
            return false;
        }
        else {
            ModelLogin modelLogin = new ModelLogin(context);
           if( modelLogin.CheckUser(username,password).equals(false)){
               Toast.makeText(context,"Vui lòng điền đúng thông tin",Toast.LENGTH_LONG).show();
               return  false;
           }else {
            return true;
           }
        }
    }

    private boolean CheckNullText(String username , String password){
        if(username.equals("") || password.equals(""))
            return false;
        else return true;
    }

    public List<String> getData(){
        SQLiteDatabaseHandler sqLiteDatabaseHandler = new SQLiteDatabaseHandler(context,null,null,1);
        return sqLiteDatabaseHandler.getUser();
    }
}
