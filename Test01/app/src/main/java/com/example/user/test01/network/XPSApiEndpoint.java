package com.example.user.test01.network;

import com.example.user.test01.Model.ModelOject.LoginParams;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface XPSApiEndpoint {

    @POST("/web/session/authenticate")
    Call<String> authenticate(
            @Body LoginParams param);

}
