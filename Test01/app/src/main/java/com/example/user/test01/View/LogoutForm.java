package com.example.user.test01.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.test01.Controller.Controller;
import com.example.user.test01.R;

import java.util.List;

public class LogoutForm extends AppCompatActivity implements View.OnClickListener {

    private TextView txt_uid, txt_session, txt_db , txt_username,txt_partner;
    private Button btlogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout_form);
        Init();
        setData();
    }

    private void Init(){
        txt_uid = (TextView)findViewById(R.id.txt_uid);
        txt_session = (TextView)findViewById(R.id.txt_session);
        txt_db = (TextView)findViewById(R.id.txt_db);
        txt_username = (TextView)findViewById(R.id.txt_username);
        txt_partner = (TextView)findViewById(R.id.txt_partner);
        btlogout = (Button)findViewById(R.id.btlogout);
        btlogout.setOnClickListener(this);
    }

    public void setData(){
        Controller controller = new Controller(this);
        List<String> list = controller.getData();
        txt_uid.setText("Uid: "+list.get(0));
        txt_session.setText("Session: "+list.get(1));
        txt_db.setText("DB: "+list.get(2));
        txt_username.setText("Username: "+list.get(3));
        txt_partner.setText("Partner: "+list.get(4));
    }


    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.btlogout){
            Controller  controller = new Controller(this);
            controller.deleteDataUser(txt_uid.getText().toString().trim());
            Intent intent = new Intent(this,LoginForm.class);
            startActivity(intent);
            finish();
        }
    }
}
