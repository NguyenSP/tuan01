package com.example.user.test01.Model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.user.test01.Model.ModelOject.LoginParams;
import com.example.user.test01.Model.ModelOject.ResultUserLogin;
import com.example.user.test01.Model.SQlite.SQLiteDatabaseHandler;
import com.example.user.test01.network.XPSApiEndpoint;
import com.example.user.test01.network.XPSClient;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;

public class ModelLogin {
    private Context context;
    public ModelLogin(Context context) {
        this.context = context;
    }

    public AsyncTask<Void, Void, Boolean> CheckUser(final String username , final String password){
       return new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
              return   doLogin(username,password);
            }
        }.execute();
    }

    private boolean doLogin(String username , String password) {
        XPSApiEndpoint xps = XPSClient.getXPSClient();
        try {
            Call<String> call = xps.authenticate(new LoginParams("hhd_e300_vn_test01", username, password));
            JSONObject objectResponse = new JSONObject(call.execute().body());
            if(objectResponse.has("result")) {
                Gson gson = new Gson();
                ResultUserLogin user = gson.fromJson(objectResponse.getString("result"), ResultUserLogin.class);
                setSession(user.getSession_id().trim());
                addSQLite(user.getSession_id(), user.getUid(),user.getUser_context(),user.getDb(),user.getUsername(),user.getCompany_id(),user.getPartner_id());
                return  true;
            } else return false;
        } catch (Exception e) {
            Log.d("error", "Authenticate Error: " + e.toString());
            return false;
        }
    }

    private void addSQLite(String session , String uid , ResultUserLogin.UserContext userContext , String db , String username , String company , String partner){
        SQLiteDatabaseHandler sqLiteDatabaseHandler = new SQLiteDatabaseHandler(context,null,null,1);
        sqLiteDatabaseHandler.Create();
        if(sqLiteDatabaseHandler.CheckUpdate(uid)== true)
            sqLiteDatabaseHandler.Update(session,uid,userContext,db,username,company,partner);
        else
        sqLiteDatabaseHandler.addUser(session,uid,userContext,db,username,company,partner);
    }


    public void setSession(String session){
        XPSClient xpsClient = new XPSClient();
        xpsClient.session_id=session;
    }

    public void Logout(String session){
        setSession(session);
    }

}
