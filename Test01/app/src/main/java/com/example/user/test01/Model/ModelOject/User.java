package com.example.user.test01.Model.ModelOject;

public class User  {
    private String username;
    private String uid;
    private String company_id;
    private String partner_id;

    public User(String username, String uid, String company_id, String partner_id) {
        this.username = username;
        this.uid = uid;
        this.company_id = company_id;
        this.partner_id = partner_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }
}
