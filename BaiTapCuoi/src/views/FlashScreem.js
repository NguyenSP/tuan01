import React, {Component} from 'react';
import {View, ImageBackground} from 'react-native';
import logo from '../assets/images/logoSieuThiNhaDat.png';

export default class FlashScreem extends Component{
    render(){
        return (  
             <ImageBackground source={logo} style={{width: '100%', height: '100%'}} />
          );
    }
    componentDidMount(){
        setTimeout(()=>{
            const {navigation} = this.props
            navigation.navigate("LoginScreen")
        },3000)
    }
}