import React, {Component} from 'react';
import {View, ImageBackground, Dimensions,StyleSheet, Animated} from 'react-native';
import ToolbarApp from '../components/ToolbarApp';
import NavigationBottomTrangChu from '../../utils/NavigationBottomTrangChu';

import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import TinDangView from './TinDang';
import DuAnView from './DuAn/DuAn';

export default class TrangChu extends Component{
   
    state = {
        index: 0,
        routes: [
          { key: 'DuAn', title: 'Dự Án' },
          { key: 'TinDang', title: 'Tin Đăng' },
        ],
      };

      _renderLabel = props => ({ route, index }) => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        const outputRange = inputRange.map(
          inputIndex => (inputIndex === index ? 'green' : 'black')
        );
        const color = props.position.interpolate({
          inputRange,
          outputRange,
        });
    
        return (
          <Animated.Text style={[{fontSize: 13,
            fontWeight: 'bold',
            margin:4 }, { color }]}>
            {route.title}
          </Animated.Text>
        );
      };
    


    render(){
        return (  
            <View style={{
                flexDirection: 'column',
                height: '100%',
                backgroundColor: '#73ba5e',
            }}>
                <ToolbarApp/>

                <TabView 
                    navigationState={this.state}
                    renderScene={SceneMap({
                       DuAn: () => (   <DuAnView navigation ={this.props.navigation} />  ),
                       TinDang: () => ( <TinDangView />)
                    })}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width,height: Dimensions.get('window').height -60}}
                    renderTabBar={props =>
                        <TabBar
                          {...props}
                          indicatorStyle={{ backgroundColor: 'green' }}
                          style ={{backgroundColor:'#fff'}}
                          renderLabel={this._renderLabel(props)}  /> }
                />

                <View style={{height: 60}}>
                    <NavigationBottomTrangChu/>
                </View>
            </View>
          );
    }
}
