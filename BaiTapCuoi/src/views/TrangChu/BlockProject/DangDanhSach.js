import React, {Component} from 'react';
import {View, ImageBackground,FlatList} from 'react-native';
import CellDangDanhSachBlock from '../../components/CellDangDanhSachBlock';

import {connect} from 'react-redux';


class DangDanhSach extends Component{

    _renderItem = ({ item, index }) => <CellDangDanhSachBlock item={item} index={index}/>

    render(){
      const {ProjectReducer} = this.props; // lấy danh sách block
    
        return (  
                <View style={{backgroundColor:'#eaeae1',height:'100%'}}>
                  <FlatList
                        extraData={ProjectReducer}
                        data={ProjectReducer}
                        keyExtractor={ProjectReducer.id_block} 
                        renderItem={this._renderItem} /> 
                </View>
           
          );
    }
}


function mapStateToProps(state){
  return{ProjectReducer : state.ProjectReducer};
}
  
export default connect(mapStateToProps)(DangDanhSach);




