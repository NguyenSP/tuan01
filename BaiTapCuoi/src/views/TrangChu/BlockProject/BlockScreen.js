
import React, {Component} from 'react';
import { View, Image, Animated, Dimensions, Text, TouchableHighlight, FlatList,StyleSheet} from 'react-native';

import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

import ic_action_back from '../../../assets/images/ic_action_back.png';
import ic_action_menu from '../../../assets/images/ic_action_menu.png';

import DangLuoiView from './DangLuoi';
import DangDanhSachView from './DangDanhSach';
import SoDoCanHoView from './SoDoCanHo';

import {connect} from 'react-redux';

import DuAnMoiController from '../../../controllers/DuAnController';

import {updateListBlock,updateListFloor} from '../../../redux/actionCreators';


 class BlockScreen extends Component{
   
        state = {
        index: 0,
        routes: [
          { key: 'DangLuoi', title: 'Dạng Lưới' },
          { key: 'DangDanhSach', title: 'Dạng Danh Sách' },
          { key: 'SoDoCanHo', title: 'Sơ Đồ Căn Hộ' },
        ],
      };

      _renderLabel = props => ({ route, index }) => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        const outputRange = inputRange.map(
          inputIndex => (inputIndex === index ? 'green' : 'black')
        );
        const color = props.position.interpolate({
          inputRange,
          outputRange,
        });
    
        return (
          <Animated.Text style={[{fontSize: 13,
            fontWeight: 'bold',
            margin:4 }, { color }]}>
            {route.title}
          </Animated.Text>
        );
      };

      componentWillMount(){
        const {id,name} = this.props.navigation.state.params
        const {ProjectReducer} = this.props;
        const setData = this
      

            // đưa block vào store
            DuAnMoiController._getBlockProject(id,function(dataAllBlock){
              var AllBlock = DuAnMoiController._extratAllBlock(dataAllBlock)
              setData.props.updateListBlock(AllBlock)
            },function(data){})
 
           // đưa floor vào store 
            DuAnMoiController._getBlockProject(id,function(dataBlock){
               for(i=0;i<dataBlock.length;i++){
                  DuAnMoiController._getParentID(dataBlock[i].id,function(dataFloor){
                  var AllFloor = DuAnMoiController._extratAllFloor(dataFloor)
                  setData.props.updateListFloor(AllFloor)
                  },function(dataFloor){})             
            }
            },function(dataBlock){})         
             
      }


    render(){
        const {id,name} = this.props.navigation.state.params
        return (  
            <View style={{ flexDirection: 'column',  height: '100%',}}>

                <View style={styles.itemContainer}>
                  <TouchableHighlight 
                          onPress = {() => {const {navigation} = this.props
                                            navigation.navigate("TrangChu")  }}>
                          <Image style={{width: 32, height: 32}} source={ic_action_back}/>
                  </TouchableHighlight>

                  <Text style={{fontSize: 20,  color: 'white', fontWeight: 'bold'}}>  Dự án </Text>

                  <TouchableHighlight 
                          onPress = {() => { }}>
                          <Image style={{width: 32, height: 32}}  source={ic_action_menu} />
                  </TouchableHighlight>
                </View>

                <Text style={{fontSize: 20, fontWeight: 'bold',borderWidth: 1}}>{name}</Text>

                <TabView 
                    navigationState={this.state}
                    renderScene={SceneMap({
                        DangLuoi: () => (   <DangLuoiView/>  ),
                        DangDanhSach: () => ( <DangDanhSachView id_project={id}/>),
                        SoDoCanHo:()=>(<SoDoCanHoView />)

                    })}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width,height: Dimensions.get('window').height -60}}
                    renderTabBar={props =>
                        <TabBar
                          {...props}
                          indicatorStyle={{ backgroundColor: 'green' }}
                          style ={{backgroundColor:'#fff'}}
                          renderLabel={this._renderLabel(props)} />  } />
            </View>
          );
    }
}

function mapStateToProps(state){
  return{ProjectReducer : state.ProjectReducer};
}

export default connect(mapStateToProps,{updateListBlock,updateListFloor})(BlockScreen);



const styles = StyleSheet.create({ 
  itemContainer:{ width: '100%',  height: 60,   flexDirection: 'row',
                  justifyContent: 'space-between', alignItems: 'center', 
                   backgroundColor: '#4CAF50',},
}); 
