import React, {Component} from 'react';
import {View, ImageBackground,ScrollView,Text,TouchableWithoutFeedback} from 'react-native';

export default class SoDoCanHo extends Component{

    constructor(props){
        super(props);
        this.state = {
            isBorderColorChoiceTatCa :false,
            isBorderColorChoiceChuaBan :false,
            isBorderColorChoiceGiuCho :false,
            isBorderColorChoiceDatCoc :false,
        };
    }


    _ChoiceTatCa(){
        this.setState({
            isBorderColorChoiceTatCa:true,
            isBorderColorChoiceChuaBan:false,
            isBorderColorChoiceGiuCho:false,
            isBorderColorChoiceDatCoc:false,
        })
    }

    _ChoiceChuaBan(){
        this.setState({
            isBorderColorChoiceTatCa:false,
            isBorderColorChoiceChuaBan:true,
            isBorderColorChoiceGiuCho:false,
            isBorderColorChoiceDatCoc:false,
        })
    }

    _ChoiceGiuCho(){
        this.setState({
            isBorderColorChoiceTatCa:false,
            isBorderColorChoiceChuaBan:false,
            isBorderColorChoiceGiuCho:true,
            isBorderColorChoiceDatCoc:false,
        })
    }
    

    _ChoiceDatCoc(){
        this.setState({
            isBorderColorChoiceTatCa:false,
            isBorderColorChoiceChuaBan:false,
            isBorderColorChoiceGiuCho:false,
            isBorderColorChoiceDatCoc:true,
        })
    }

    render(){
        return (  
            
                <View style={{flex:1,backgroundColor:'white'}}>
                  <View style={{height:60,width:'100%',flexDirection:'row'}}>
                  <ScrollView  horizontal={true}>
                         
                        <TouchableWithoutFeedback
                         onPress={() => this._ChoiceTatCa()}>
                                <View style ={{ borderBottomColor: this.state.isBorderColorChoiceTatCa ? '#3b2712': 'white',
                                                borderBottomWidth: 15,
                                    height:60,width:100,backgroundColor:'#3b2712',flexDirection:'column',alignItems: 'center',justifyContent: 'center'}}>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:15}} >Tất Cả</Text>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:15}}>(0)</Text>
                                </View>               
                         </TouchableWithoutFeedback>

                         <TouchableWithoutFeedback
                         onPress={() => this._ChoiceChuaBan()}>
                                <View style ={{ borderBottomColor: this.state.isBorderColorChoiceChuaBan ? '#4CAF50': 'white',
                                                borderBottomWidth: 15,
                                    height:60,width:100,backgroundColor:'#4CAF50',flexDirection:'column',alignItems: 'center',justifyContent: 'center'}}>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:15}} >Chưa bán</Text>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:15}}>(0)</Text>
                                </View>               
                         </TouchableWithoutFeedback>

                         <TouchableWithoutFeedback
                         onPress={() => this._ChoiceGiuCho()}>
                                <View style ={{ borderBottomColor: this.state.isBorderColorChoiceGiuCho ? '#4700b3': 'white',
                                                borderBottomWidth: 15,
                                    height:60,width:100,backgroundColor:'#4700b3',flexDirection:'column',alignItems: 'center',justifyContent: 'center'}}>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:15}} >Giữ chổ</Text>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:15}}>(0)</Text>
                                </View>               
                         </TouchableWithoutFeedback>


                         <TouchableWithoutFeedback
                         onPress={() => this._ChoiceDatCoc()}>
                                <View style ={{ borderBottomColor: this.state.isBorderColorChoiceDatCoc ? '#cc00cc': 'white',
                                                borderBottomWidth: 15,
                                    height:60,width:100,backgroundColor:'#cc00cc',flexDirection:'column',alignItems: 'center',justifyContent: 'center'}}>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:15}}>Đặt cọc</Text>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:15}}>(0)</Text>
                                </View>               
                         </TouchableWithoutFeedback>
                  </ScrollView>
                    

                  </View>
                </View>
           
          );
    }
}