import React, { Component } from 'react';
import {
    View, Text, Image, FlatList
} from 'react-native';
import DuAnMoiController from '../../../controllers/DuAnController';
import CellProduct from '../../components/CellProduct';

export default class ApartmentComponent extends Component{

    constructor(props){
        super(props);
        this.state = {
            list_product: [],
        };
    }

    _getlist_id_blockProject(){
        const {id_project} = this.props
        const getData = this
        DuAnMoiController._getDatalist_id_blockProject(id_project,function(data){ 

            DuAnMoiController._getProduct(data[0].structure_ids,function(data2){
                getData.setState({
                    list_product:data2
                })
            },function(data2){})
            
       },
       function(data){

       }
   );
    }

    componentWillMount(){
       this._getlist_id_blockProject()
    }

    _renderItem = ({ item, index }) => <CellProduct item={item} index={index} />

    render(){

        return(
            <View>
                <FlatList
                        style = {{
                            marginLeft: 5,
                            marginRight: 5,
                            marginTop: 5,
                            marginBottom: 10,
                        }}
                        extraData={this.state.list_product}
                        data={this.state.list_product}
                        renderItem={this._renderItem}
                    />
            </View>
         
        );
    }
}