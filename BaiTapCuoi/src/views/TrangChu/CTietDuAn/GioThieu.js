import React, {Component} from 'react';
import {View, ImageBackground,Text} from 'react-native';
import DuAnMoiController from '../../../controllers/DuAnController';

export default class GioiThieu extends Component{
   
    constructor(props){
        super(props);
        this.state = {
            nameProject:''
        };
    }


    componentWillMount(){
         const {id_project} = this.props
        const setData = this
        DuAnMoiController._getDataDetailProject(id_project,function(data){
                 setData.setState({
                    nameProject: data[0].name
                 })
            },
            function(data){

            }
        );
    }

    render(){
        return (  
            
                <View style={{flex:1}}>
                     <Text >{this.state.nameProject}</Text>
                </View>
           
          );
    }
}