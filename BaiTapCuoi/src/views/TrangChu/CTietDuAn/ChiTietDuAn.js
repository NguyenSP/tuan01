
import React, {Component} from 'react';
import {View, ImageBackground, Dimensions,StyleSheet, Animated,Image,Text,TouchableWithoutFeedback} from 'react-native';

import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import image_khudancu from '../../../assets/images/khudancu.jpg';
import ic_action_back from '../../../assets/images/ic_action_back.png';

import GioiThieuView from './GioThieu';
import ViTriView from './ViTri';
import CanHoView from './CanHo';
import LienHeView from './LienHe';

export default class TrangChu extends Component{
   
    state = {
                index: 0,
                routes: [
                  { key: 'GioiThieu', title: 'Giới Thiệu' },
                  { key: 'CanHo', title: 'Căn Hộ' },
                  { key: 'ViTri', title: 'Vị trí' },
                  { key: 'LienHe', title: 'Liên hệ' },
                ],
              };

      _renderLabel = props => ({ route, index }) => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        const outputRange = inputRange.map(
          inputIndex => (inputIndex === index ? 'black' : 'green')
        );
        const color = props.position.interpolate({
          inputRange,
          outputRange,
        });
    
        return (
          <Animated.Text style={[{fontSize: 13,
            fontWeight: 'bold',
            margin:4 }, { color }]}>
            {route.title}
          </Animated.Text>
        );
      };


      _onBackDuAnMoi(){
        const {navigation} = this.props;
        navigation.navigate("TrangChu")
      }
    
    render(){
        const {id,name} = this.props.navigation.state.params
        return (  
            <View style={{  flexDirection: 'column',   height: '100%',  }}>

                <View style={styles.itemLittle}>
                   <Image  style={styles.image_logo} source={image_khudancu}  />
                   <View style={{position:'absolute'}}>
                      <Text style={{fontWeight: 'bold',fontSize: 16,color :'green'}} >{name}</Text>
                   </View>
                </View>

                <TouchableWithoutFeedback
                    onPress={() => this._onBackDuAnMoi()}>
                    <Image style={{ position: 'absolute',marginTop:10,height:50,width:50}} source={ic_action_back}/>
                </TouchableWithoutFeedback>

                    
                <TabView  style={{borderWidth: 2,  borderColor: 'blue'}}
                    navigationState={this.state}
                    renderScene={SceneMap({
                        GioiThieu: () => (   <GioiThieuView id_project ={id} />  ),
                        CanHo: () => ( <CanHoView  id_project ={id} />),
                        ViTri: () => ( <ViTriView id_project ={id}  />),
                        LienHe: () => ( <LienHeView />),
                    })}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width,height: Dimensions.get('window').height -60}}
                    renderTabBar={props =>
                        <TabBar style={{borderWidth: 2,  borderColor: 'blue'}}
                          {...props}
                          indicatorStyle={{ backgroundColor: 'green' }}
                          style ={{backgroundColor:'#fff'}}
                          renderLabel={this._renderLabel(props)}
                        />
                      }
                />
                   <View style={styles.itemBottom}>
                      <View style={styles.itemBottomChild}>
                        <Text style={styles.text}>GỬI THÔNG TIN</Text>
                      </View>

                      <View style={styles.itemBottomChild}>
                        <Text style={styles.text}>ĐẶT LỊCH THAM QUAN DỰ ÁN</Text>
                      </View>

                      <View style={styles.itemBottomChild}>
                       <Text style={styles.text}>ĐĂNG KÍ HỖ TRỢ</Text>
                      </View>
                </View>
            </View>
          );
    }
}


const size = Dimensions.get('window').width; 
const sizeHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({ 
    itemLittle:{height:sizeHeight/4,flexDirection: 'column', justifyContent: 'center', alignItems: 'center'},
    image_logo: {width:size, resizeMode: 'contain',position:'relative'},
    itemBottom:{height: 60, flexDirection: 'row',alignItems:'center',justifyContent:'center'},
    itemBottomChild:{backgroundColor:'#009999',height:60,width:size/3,alignItems:'center',justifyContent:'center'},
    text:{fontWeight: 'bold',color:'white',fontSize:15, textAlign: 'center'}
}); 