import React, {Component} from 'react';
import {View, ImageBackground,FlatList,Dimensions, StyleSheet,Text} from 'react-native';
import DuAnMoiController from '../../../controllers/DuAnController';
import DuAnMoiCustomer from '../../components/DuAnMoiCustomer';

export default class DuAnMoi extends Component{
    constructor(props){
        super(props);
        this.state = {
            list_category: []
        };
    }

    componentWillMount(){
        const setData = this
        DuAnMoiController._getDataCategory(function(data){
                 setData.setState({
                    list_category: data 
                 })
            },function(data){}
        );
    }

    _renderItem = ({ item, index }) => <DuAnMoiCustomer item={item} index={index} navigation = {this.props.navigation} />

    render(){
        return (  
                <View>
                    <FlatList
                        extraData={this.state.list_category}
                        data={this.state.list_category}
                        keyExtractor={this.state.list_category.id} 
                        renderItem={this._renderItem} /> 
                </View>
           
          );
    }
}