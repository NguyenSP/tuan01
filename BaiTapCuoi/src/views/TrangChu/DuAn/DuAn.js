import React, {Component} from 'react';
import {View, ImageBackground, Dimensions,StyleSheet,Animated} from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import DuAnMoiView from './DuAnMoi';
import LocDuAnView from './LocDuAn';
import DangDuAnView from './DangDuAn';


export default class DuAn extends Component{
   
    state = {
        index: 0,
        routes: [
          { key: 'DuAnMoi', title: 'Dự Án Mới' },
          { key: 'LocDuAn', title: 'Lọc Dự Án' },
          { key: 'DangDuAn', title: 'Đăng Dự Án' },
        ],
      };

      _renderLabel = props => ({ route, index }) => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        const outputRange = inputRange.map(
          inputIndex => (inputIndex === index ? 'green' : 'black')
        );
        const color = props.position.interpolate({
          inputRange,
          outputRange,
        });
    
        return (
          <Animated.Text style={[{fontSize: 13,
            fontWeight: 'bold',
            margin:4 }, { color }]}>
            {route.title}
          </Animated.Text>
        );
      };

    render(){
        return (  
            <View style={{height: '100%'}}>
                <TabView
                    navigationState={this.state}
                    renderScene={SceneMap({
                        DuAnMoi: () => ( <DuAnMoiView navigation ={this.props.navigation} />),
                        LocDuAn: () => (   <LocDuAnView/> ),
                        DangDuAn : () => (   <DangDuAnView/>)
                    })}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width,height: Dimensions.get('window').height -60}}
                    renderTabBar={props =>
                        <TabBar
                          {...props}
                          indicatorStyle={{ backgroundColor: 'green' }}
                          style ={{backgroundColor:'#fff'}}
                          renderLabel={this._renderLabel(props)}
                        />
                      }
                />
            </View>
          );
    }
}
