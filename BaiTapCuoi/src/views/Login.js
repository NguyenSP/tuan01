import React, { Component } from "react";
import { View, TextInput, TouchableWithoutFeedback, Text,Image,
} from "react-native";
import styles from "../themes/styles";
import LoginController from '../controllers/LoginController';
import cssLogin from '../themes/cssLogin';
import image_logo_hhd from '../assets/images/image_logo.png';
import CheckBox from 'react-native-check-box';

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: 'hhdtest1@hhdgroup.com',
            password: '123123123',
            uid: ''
        };
    }
    _onPressLogin(){
        const {navigation} = this.props
        LoginController._onPressLogin(navigation ,this.state.email,this.state.password)
    }

    render() {
        const { image_logo} = cssLogin;
        const {container,textInput,textButton}= styles;
        return (
            <View style={container}>
                <View style={{alignItems:'center'}}>
                    <Image  style={image_logo} source={image_logo_hhd}  />
                </View>
                <View>
                <TextInput
                    style={textInput}
                    placeholder='User Name'
                    keyboardType='email-address'
                    onChangeText={(text) => {
                        this.setState(() => {
                            return {
                                email: text
                            };
                        })
                    }}
                    value={this.state.email}></TextInput>
                </View>
                <View>
                <TextInput
                    style={[textInput, { margin: 20 }]}
                    placeholder='Password'
                    keyboardType='default'
                    secureTextEntry={true}
                    onChangeText={(text) => {
                        this.setState(() => {
                            return {
                                password: text
                            };
                        })
                    }}
                    value={this.state.password}></TextInput>
                </View>

                <TouchableWithoutFeedback
                    onPress={() => this._onPressLogin()}>
                    <Text style={textButton}>Login</Text>
                </TouchableWithoutFeedback>

                <View style={{flexDirection: 'row',width:300,height:50,marginTop:20}}>
                <CheckBox
                    onClick={()=>{
                    this.setState({  isChecked:!this.state.isChecked })
                    }}
                    isChecked={this.state.isChecked} 
                    checkBoxColor='white' />
                    <Text style={{color:'white',marginLeft:10}}>Remember me</Text>
               </View>

               <View style={{flexDirection: 'row',width:300,height:50}}>
                    
                    <Text style={{color:'white',marginLeft:10}}>Not a remember ?</Text>

                    <TouchableWithoutFeedback
                    onPress={() => this._onPressLogin()}>
                  <Text style={{color:'white',marginLeft:10,  
                    textDecorationLine: "underline",
                    textDecorationStyle: "solid",
                    textDecorationColor: "#000"}}>Sign up now</Text>
                </TouchableWithoutFeedback>
               </View>

            </View>
        );
    }
}

