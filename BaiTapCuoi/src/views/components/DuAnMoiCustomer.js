import React, { Component } from 'react';
import { View, Text,StyleSheet,Dimensions,Image,TouchableWithoutFeedback,FlatList} from 'react-native';
import DuAnMoiController from '../../controllers/DuAnController';
import CellCategoryProject from '../components/CellCategoryProject';
import styles from '../../themes/styles'

export default class DuAnMoiCustomer extends Component {
 

    constructor(props){
        super(props);
        this.state = {
            list_project: []
        };
    }

    componentWillMount(){
        const {item, index} = this.props
        const setData = this
        DuAnMoiController._getDataProject(item.id,function(data){
                 setData.setState({
                    list_project: data
                 })
            }, function(data){}
        );
    }

    _renderItem = ({ item, index }) => <CellCategoryProject item={item} index={index} navigation={this.props.navigation}/>

    render(){
        const {item, index} = this.props
        return(
            <View style={{   flexDirection: 'column',  backgroundColor: 'white',}}>
                <Text style = { styles.textNameTopicProject }>{item.name}</Text>
                         <FlatList
                            style = {{  marginTop: 5, marginBottom: 20}}
                            extraData={this.state.list_project}
                            keyExtractor={this.state.list_project.id} 
                            data={this.state.list_project}
                            renderItem={this._renderItem}
                            numColumns={2}
                        /> 
            </View>
        )
    }
}
