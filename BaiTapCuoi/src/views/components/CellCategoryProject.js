import React, { Component } from 'react';
import { 
    View, Text, FlatList, Image, TouchableWithoutFeedback,Dimensions,StyleSheet
} from 'react-native';
import image_khudancu from '../../assets/images/khudancu.jpg';
export default class CellCategoryProject extends Component {

  
    _onPressChiTietDuAn(){
        const {navigation} = this.props;
        const {item, index} = this.props
        navigation.navigate("ChiTietDuAn",{id:item.id,name:item.name})
    }

    _onPressBlockDuAn(){
        const {navigation} = this.props;
        const {item, index} = this.props
        navigation.navigate("BlockDuAn",{id:item.id,name:item.name})
    }
    
    render(){
        const {item, index} = this.props      
        return(
                <View style={styles.itemContainer}> 
                    <View style={{flexDirection: 'row',justifyContent: 'flex-end',width: size}}>
                        <Image  style={styles.image_logo} source={image_khudancu}  />
                        <View style={styles.money}>
                            <Text>Từ :</Text>
                            <View style={{flexDirection: 'row',justifyContent: 'flex-end'}}> 
                                <Text style={{color:'#ff471a'}}>2.4 tỉ</Text>    
                                <Text >/76m2</Text>   
                            </View>
                        </View>     
                    </View>
                      
                    <Text style={styles.itemTitle}>{item.name}</Text> 

                    {item.location===false?<Text style={styles.itemND}>Quận 9 , HCM</Text>
                                        :<Text style={styles.itemND}>{item.location}</Text> }

                    <View style={{flex: 0.5, flexDirection: 'row',justifyContent:'space-around', width: size}}>
                        <TouchableWithoutFeedback
                                    onPress={() => this._onPressChiTietDuAn()}>
                                    <Text style={{width: 70, height: 25,  fontWeight: 'bold',
                                                        borderWidth: 2,  borderColor: '#ff471a'}}> Chi Tiết </Text>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => this._onPressBlockDuAn()}>
                            <Text style={{width: 70, height: 25,color:'green',  fontWeight: 'bold',
                                                        borderWidth: 2,  borderColor: '#ff471a'}}> Mua Ngay </Text>
                        </TouchableWithoutFeedback>
                </View>           
                  </View> 
        )
    }
}


const size = Dimensions.get('window').width/2 -15; 

const styles = StyleSheet.create({ 
    itemContainer: { width: size, height: size +60,  
                     margin:5 ,flex :1 , marginTop:20,
                     justifyContent: 'flex-start', 
                     backgroundColor: 'white',},

    itemTitle: {     width: size,  fontWeight: 'bold',
                     justifyContent: 'center',alignItems:'center',
                     marginLeft:5 ,   fontSize: 15 },

    itemND: {       width: size,justifyContent: 'center',
                     alignItems:'center' ,marginLeft:5,   fontSize: 13 },

    image_logo: {   width: size, height: size-20, resizeMode: 'contain'},

    money:{          backgroundColor: 'white', position: 'absolute',
                    marginTop:50, borderBottomWidth: 2,  borderBottomColor: '#ff471a'},     
}); 