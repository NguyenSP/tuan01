import React, { Component } from 'react'
import {
    View, Text, Image, TouchableHighlight,TouchableWithoutFeedback
} from 'react-native'
import imageBarSTND from '../../assets/images/imageBarSTND.jpg';
import searchBar from '../../assets/images/ic_action_search.png';
import menuBar from '../../assets/images/ic_action_menu.png';

export default class ToolbarApp extends Component {

  render() {
    return (
      <View style={{  height: 60,   flexDirection: 'row',  justifyContent: 'flex-end',  alignItems: 'center', }}>

            <TouchableWithoutFeedback
                style = {{ marginLeft: 10, marginTop: 20, marginBottom: 20,}}
                onPress = {() => {  }}>
                <Image style={{width: 290, height: 32}} source={imageBarSTND}/>
            </TouchableWithoutFeedback>


            <TouchableWithoutFeedback
                style = {{ marginRight: 10, marginTop: 20, marginBottom: 20,}}
                onPress = {() => { }}>
                <Image style={{width: 32, height: 32}}   source={searchBar}/>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
                style = {{ marginRight: 10, marginTop: 20, marginBottom: 20,}}
                onPress = {() => { }}>
                <Image  style={{width: 32, height: 32}} source={menuBar} />
            </TouchableWithoutFeedback>

      </View>
    );
  }
}
