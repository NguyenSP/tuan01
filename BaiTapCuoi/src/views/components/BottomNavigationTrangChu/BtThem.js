import React, { Component } from 'react';
import {
    View, Text, Image
} from 'react-native';
import icon from '../../../assets/images/ic_action_more.png';

export default class BtThem extends Component {

    static navigationOptions = ({navigation}) => {
        const { params = {}} = navigation.state;
        let tabBarLabel = 'Thêm';
        let tabBarIcon = () => (
            <Image
                source = {icon}
                style = {{width: 28, height: 28,}}
            />
        );
        return { tabBarLabel, tabBarIcon };
    }

    render(){
        return(
            <View>
                <Text>
                    Màn hình Thêm
                </Text>
            </View>
        );
    }
}