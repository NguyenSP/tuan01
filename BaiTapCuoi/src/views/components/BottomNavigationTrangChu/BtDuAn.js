import React, { Component } from 'react';
import {
    View, Text, Image,Button,TouchableWithoutFeedback , TouchableHighlight
} from 'react-native';
import icon from '../../../assets/images/ic_action_project.png';

export default class BtDuAn extends Component {

    static navigationOptions = ({navigation}) => {
        const { params = {}} = navigation.state;
        let tabBarLabel = 'Dự Án';
        let tabBarIcon = () => (
            <Image
                source = {icon}
                style = {{width: 28, height: 28,}}
            />
        );
        return { tabBarLabel, tabBarIcon };
    }

    render(){
        return(
            <View>
                <Text>
                    Màn hình dự án
                </Text>
            </View>
        );
    }
}