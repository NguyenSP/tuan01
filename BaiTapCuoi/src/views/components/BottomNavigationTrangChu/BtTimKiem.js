import React, { Component } from 'react';
import {
    View, Text, Image
} from 'react-native';
import icon from '../../../assets/images/ic_action_search_black.png';

export default class BtTimKiem extends Component {

    static navigationOptions = ({navigation}) => {
        const { params = {}} = navigation.state;
        let tabBarLabel = 'Tìm Kiếm';
        let tabBarIcon = () => (
            <Image
                source = {icon}
                style = {{width: 28, height: 28,}}
            />
        );
        return { tabBarLabel, tabBarIcon };
    }

    render(){
        return(
            <View>
                <Text>
                    Màn hình tìm kiếm
                </Text>
            </View>
        );
    }
}