import React, { Component } from 'react';
import { 
    View, Text, FlatList, Image, TouchableWithoutFeedback,Dimensions,StyleSheet
} from 'react-native';
import image_khudancu from '../../assets/images/khudancu.jpg';
import DuAnMoiController from '../../controllers/DuAnController';
import CellDangDanhSachProduct from '../components/CellDangDanhSachProduct';

export default class CellDangDanhSachFloor extends Component {

    constructor(props){
        super(props);
        const {item, index} = this.props
        this.state={
            list_product: [],
            nameFloor:item.name
        }
    }

    componentWillMount(){
        const {item, index} = this.props
          const setData = this
          DuAnMoiController._getProductForFloor(item.id_floor,function(data){
              setData.setState({
                list_product: data
               })
            
          },function(data){})
       }


      _renderItem = ({ item, index }) => <CellDangDanhSachProduct item={item} floor={this.state.nameFloor} />

    render(){
        const {item, index} = this.props
        const size = Dimensions.get('window').width/2 -15; 
        return(
                  
                   <FlatList
                        extraData={this.state.list_product}
                        data={this.state.list_product}
                        keyExtractor={this.state.list_product.id} 
                        renderItem={this._renderItem} /> 
              
        )
    }
}


