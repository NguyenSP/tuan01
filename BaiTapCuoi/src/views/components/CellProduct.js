import React, { Component } from 'react';
import { 
    View, Text, FlatList, Image,StyleSheet
} from 'react-native';
import image_canho from '../../assets/images/canho.jpg';
import image_car from '../../assets/images/ic_action_car.png';
import image_store from '../../assets/images/ic_action_store.png';
import image_flag from '../../assets/images/ic_action_flag.png';
export default class CellProduct extends Component {

    render(){
        const {item, index} = this.props
        return(
            <View style={styles.itemContainer}>
               <Image source={image_canho}  style={styles.image}/>
               <View style={styles.itemInfor}>
                    <Text style = {styles.text}>
                    {item.name}</Text>
                    <Text style={{   fontSize: 11,    marginLeft: 8 }}>     Liên hệ     </Text>
                    <Text style={{  height: 0.4, width: '100%',   marginTop: 5,  backgroundColor: 'gray', }}> </Text>
                    <View style={{ flexDirection: 'row',    alignItems: 'center',   marginTop: 5, }}>

                    <Image source={image_car}
                        style={{height: 14, width: 20}}/>

                        <Text> 1</Text>

                        <Image source={image_store}
                        style={{marginLeft: 30, height: 15, width: 20}}/>

                        <Text> 1</Text>

                        <Image source={image_flag}
                        style={{marginLeft: 30, height: 15, width: 20}}/>

                    </View>
               </View>
                
            </View>
        )
    }
}


const styles = StyleSheet.create({ 
    itemContainer:{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginBottom: 10,
                                        },

    image: {  width: 100,    height: 70,    backgroundColor: 'gray' },

    itemInfor: {    flexDirection: 'column', 
                    marginLeft: 10,  width: '100%',
                   justifyContent: 'flex-start',
                     alignItems: 'flex-start',   },

     text :{   width: '100%', 
                 color: '#4CAF50',   
               fontSize: 16,    
                fontWeight: 'bold',  
             marginLeft: 8,   },   
}); 