import React, { Component } from 'react';
import { 
    View, Text, FlatList, Image, TouchableWithoutFeedback,Dimensions,StyleSheet
} from 'react-native';


import image_canho from '../../assets/images/canho.jpg';
import image_car from '../../assets/images/ic_action_car.png';
import image_store from '../../assets/images/ic_action_store.png';
import image_flag from '../../assets/images/ic_action_flag.png';

export default class CellDangDanhSachProduct extends Component {

    render(){
        const {item, index,floor} = this.props

        return( 
                 <View style={styles.itemContainer}>
                   <Image source={image_canho}  style={styles.image}/>
                   <View style={styles.itemInfor}>
                    
                        <View style={{ flexDirection: 'row',    alignItems: 'flex-start',   marginTop: 5, }}>
                                <Text style={styles.text} >Căn hộ </Text>
                                <Text style={styles.text}>{item.name}</Text>
                                <Text style={styles.text} >{' '+floor}</Text>
                        </View>

                        <View style={{ flexDirection: 'row',    alignItems: 'flex-start',   marginTop: 5, }}>
                                <Text style={{color:'red',fontWeight: 'bold',fontSize: 13}}>980tr</Text>
                                <Text style={styles.text} > /căn</Text>
                        </View>

                        <Text style={{  height: 1, width: '100%',   marginTop: 5,  backgroundColor: 'gray', }}> </Text>
                      
                        <View style={{ flexDirection: 'row',    alignItems: 'center',   marginTop: 5, }}>
                                <Image source={image_car}   style={{height: 14, width: 20}}/>
                                <Text> 1</Text>
                                <Image source={image_store} style={{marginLeft: 30, height: 15, width: 20}}/>
                                <Text> 1</Text>
                                <Image source={image_flag} style={{marginLeft: 30, height: 15, width: 20}}/>
                        </View>

                   </View>
                    
                </View>
        )
    }
}




const size = Dimensions.get('window').width/2 -15; 

const styles = StyleSheet.create({ 
    itemContainer: {    flexDirection: 'row',   
                        justifyContent: 'flex-start',   
                        alignItems: 'center',  
                        margin: 5,
                        backgroundColor:'white'},

    image: {  width: 100,    height: 70,    backgroundColor: 'gray' },

    itemInfor: {    flexDirection: 'column',
                    marginLeft: 10,
                    width: '100%', 
                    justifyContent: 'flex-start',  
                    alignItems: 'flex-start',   },
     text :{fontWeight: 'bold',fontSize: 13},   
}); 