import React, { Component } from 'react';
import { 
    View, Text, FlatList, Image, TouchableWithoutFeedback,Dimensions,StyleSheet
} from 'react-native';

import CellDangDanhSachFloor from '../components/CellDangDanhSachFloor';

import {connect} from 'react-redux';

class CellDangDanhSachBlock extends Component {

    constructor(props){
        super(props);
        this.state={
            list_floor: []
        }
    }
    _renderItem = ({ item, index }) => <CellDangDanhSachFloor item={item}/>

    componentWillMount(){
        const {item, index} = this.props
        const {FloorReducer} = this.props; // lấy danh sách block
        const arr =[]
        for(i=0;i<FloorReducer.length;i++){
            if(FloorReducer[i].id_block===item.id_block)
               arr[i] =FloorReducer[i]
        }
        this.setState({
            list_floor : arr
        })
      }

    render(){
        const {item, index} = this.props
        const size = Dimensions.get('window').width; 
        return(
               
                 <View style={{flexDirection:'column'}}>

                     <View style={{flexDirection:'row', justifyContent: 'flex-start',    alignItems: 'center'}} >
                        <Text>{item.display_name}</Text>
                         <Text style={{height:2,width:size,backgroundColor:'gray'}}></Text>
                     </View>
                     
                    <FlatList
                    extraData={this.state.list_floor}
                    data={this.state.list_floor}
                    keyExtractor={this.state.list_floor.id_floot} 
                    renderItem={this._renderItem} />  
                 </View>      
        )
    }
}


function mapStateToProps(state){
    return{FloorReducer : state.FloorReducer};
  }
    
  export default connect(mapStateToProps)(CellDangDanhSachBlock);