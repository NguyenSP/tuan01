
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import BtTrangChu from "../views/components/BottomNavigationTrangChu/BtTrangChu";
import BtDuAn from '../views/components/BottomNavigationTrangChu/BtDuAn';
import BtTimKiem from '../views/components/BottomNavigationTrangChu/BtTimKiem';
import BtLuu from '../views/components/BottomNavigationTrangChu/BtLuu';
import BtThem from '../views/components/BottomNavigationTrangChu/BtThem';

let routeConfig = {
    NaviBtTrangChu: {screen: BtTrangChu },
    NaviBtDuAn: {screen: BtDuAn },
    NaviBtTimKiem: {screen: BtTimKiem},
    NaviBtLuu: {screen: BtLuu},
    NaviBtThem: {screen: BtThem}
}

let tabNavigatorConfig = {
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions: {
        activeTintColor: '#4CAF50',
        inactiveTintColor: 'black',
        renderIndicator: () => null,
        pressColor: '#76bd61',
        upperCaseLabel: false,
        labelStyle: {
            fontSize: 8.5,
            fontWeight: 'bold'
        },
        style: {
            backgroundColor: '#f2f0f0',
            height: 60,
            color: 'black'
        },
        iconStyle: {
            color: '#4CAF50'
        },
        showIcon: true
    }
}

export default AppContainer = createAppContainer(createBottomTabNavigator (routeConfig, tabNavigatorConfig));
