import { createStackNavigator, createAppContainer,createTabNavigator,createSwitchNavigator,createMaterialTopTabNavigator} from 'react-navigation';
import Login from '../views/Login';
import Flash from '../views/FlashScreem';
import TrangChu from '../views/TrangChu/TrangChu';
import ChiTietDuAn from '../views/TrangChu/CTietDuAn/ChiTietDuAn';
import BlockDuAn from '../views/TrangChu/BlockProject/BlockScreen';
const navConfig = createStackNavigator({
  
  FlashScreen:{screen:Flash},
  LoginScreen: { screen: Login },
  TrangChu: { screen: TrangChu },
  BlockDuAn:{screen:BlockDuAn},
  ChiTietDuAn :{screen:ChiTietDuAn},
  },
  {
    // ẩn thanh bar
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  }
)
const AppContainer = createAppContainer(navConfig)

export default AppContainer