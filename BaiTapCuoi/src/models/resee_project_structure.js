import Model from './model';
import { context } from '../utils/config';

export default class ReeProjectStructure extends Model{

    static id = ''
    static name = ''
    static parent_id = ''
    static type = ''

    getObjectName() {
        return "ree.project.structure";
    }
   
    getAllBlock(id_project, success, fail){
        return this.search_read([["project_id", "=", id_project],["type","=","block"]],["name","display_name"], 0, 0, "", context, success, fail);
    }



    getParentID(id_block, success, fail){
        return this.search_read([["parent_id", "=",id_block]],["id","display_name","name","parent_id"], 0, 0, "", context, success, fail);
    }
    
}