import Model from './model';
import { context } from '../utils/config';

export default class ReeProject extends Model{

    static name = '';
    static categ_ids = '';
    static description = '';
    static image_small = '';
    static investor_ids = '';
    static structure_ids = '';
    static parent_id = '';
    static summary = '';
    static location = '';
    static stage_ids = '';
    static acreage =76;
    static money = 2.1;

    getObjectName() {
        return "ree.project";
    }

    getObject(id_category,success, fail){
      
        return this.search_read([["categ_ids", "in",  id_category]],[], 0, 0, "", context, success, fail);
    }

    getInfo(id_project, success, fail){
        return this.read([id_project],[], context, success, fail);
    }

    getlist_id_blockProject(id_project, success, fail){
        return this.read([id_project],["structure_ids"], context, success, fail);
    }
}