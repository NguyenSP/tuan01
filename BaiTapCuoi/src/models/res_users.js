import Model from './model'
import api from '../models/api/api'
import { db, context } from '../utils/config'
import res_parner from '../models/res_partner'
export default class ResUsers extends Model {
    
    static name = ""
    static email = ""
    static session_id = ""
    static uid = 0
    static avatar = ""

    getObjectName() {
        return "res.users"
    }
    
    login(user_name, password, success, fail) {
        const agrs = { "db": db, "login": user_name, "password": password }
        api.callJsonRpc(agrs, success, fail, "web/session/authenticate")
    }

    logout(success, fail) {
        const agrs = {}
        api.callJsonRpc(agrs, success, fail, "web/session/destroy")
    }
    getSessionInfo(success, fail) {
        const agrs = {}
        api.callJsonRpc(agrs, success, fail, "web/session/get_session_info")
    }
    
    getMyCustomer(success, fail){
        const partner = new res_parner()
        return partner.search_read([["user_id", "=", this.uid]], ["name", "email"], 0, 10, "name ASC", context, success, fail)
    }
}