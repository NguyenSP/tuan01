import Model from './model'
import api from '../models/api/api'
import { db, context } from '../utils/config'

export default class Product extends Model {

    static name = ''
    image_small = ''  
    full_name = ''
    code = ''
    default_code = '' 
    description = ''
    ree_structure_id = ''
    ree_status = ''
    ree_position = 0
    ree_type = ''
    ree_sold = false
    lst_price = 0.0
    barcode = ''
    attribute_line_ids = ''
    name_template = ''
    product_tmpl_id = '';

    getObjectName() {
        return "product.product";
    }

    getProduct(list_id_block, success, fail){
        return this.search_read([["ree_structure_id", "child_of", list_id_block]],["name"], 0, 0, "", context, success, fail);
    }

    getProductForFloor(id_floor, success, fail){
        return this.search_read([["ree_structure_id", "=", id_floor]],["name"], 0, 0, "", context, success, fail);
    }
}