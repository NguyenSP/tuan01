import { } from 'react-native';
import ApiService from './api/api'
import { kwargs } from '../utils/config'
export default class Model {
    getObjectName() {
        return "";
    }

    search_read(domain, fields, offset = 0, limit = 80, order = "", context, success, failed) {
       
        const args = {
            "model": this.getObjectName(),
            "method": "search_read",
            "args": [domain, fields, offset, limit, order],
            "kwargs": kwargs,
            "context": context
        }
     
        ApiService.callJsonRpc(args, function (data) {
      
            success(data)
        }, function (data) {
            failed(data)
        })
    }


    read(ids, fields, context, success, failed) {
        let args = {
            "model": this.getObjectName(),
            "method": "read",
            "args": [ids, fields],
            "kwargs": kwargs,
            "context": context
        }
        ApiService.callJsonRpc(args, function (data) {
            success(data)
        }
            , function (data) {
                failed(data)
            })
    }

    // create(vals, context, success, failed, kwargs = kwargs) {
    //     let args = {
    //         "model": this.getObjectName(),
    //         "method": "create",
    //         "args": vals,
    //         "kwargs": kwargs,
    //         "context": context
    //     }

    //     ApiService.callJsonRpc(args, function (data) {
    //         success(data)
    //     }
    //         , function (data) {
    //             failed(data)
    //         })
    // }

    // write(ids, vals, context, success, failed) {
    //     const args = {
    //         "model": this.getObjectName(),
    //         "method": "write",
    //         "args": [ids, vals],
    //         "kwargs": kwargs,
    //         "context": context
    //     }
    //     ApiService.callJsonRpc(args, function (data) {
    //         success(data)
    //     }
    //         , function (data) {
    //             failed(data)
    //         })
    // }

    // search(domain, fields, offset = 0, limit = 80, order = "", context, success, failed) {

    //     const args = {
    //         "model": this.getObjectName(),
    //         "method": "search",
    //         "args": [domain, fields, offset],
    //         "kwargs": kwargs,
    //         "context": context
    //     }
    //     ApiService.callJsonRpc(args, function (data) {
    //         success(data)
    //     }
    //         , function (data) {
    //             failed(data)
    //         })
    // }
    // search_count(domain, context, success, failed) {

    //     const args = {
    //         "model": this.getObjectName(),
    //         "method": "search_count",
    //         "args": [domain],
    //         "kwargs": kwargs,
    //         "context": context
    //     }
    //     ApiService.callJsonRpc(args, function (data) {
    //         success(data)
    //     }
    //         , function (data) {
    //             failed(data)
    //         })
    // }
    // unlink(ids, context, success, failed) {
    //     const args = {
    //         "model": this.getObjectName(),
    //         "method": "unlink",
    //         "args": [ids],
    //         "kwargs": kwargs,
    //         "context": context
    //     }
    //     ApiService.callJsonRpc(args, function (data) {
    //         success(data)
    //     }
    //         , function (data) {
    //             failed(data)
    //         })
    // }

    // call_method(method, params, context, success, failed) {
    //     const args = {
    //         "model": this.getObjectName(),
    //         "method": method,
    //         "args": params,
    //         "kwargs": kwargs,
    //         "context": context
    //     }
    //     ApiService.callJsonRpc(args, function (data) {
    //         success(data)
    //     }
    //         , function (data) {
    //             failed(data)
    //         })
    // }

    // create_all(method, data, paramsKwargs, context, success, failed) {
    //     const args = {
    //         "model": this.getObjectName(),
    //         "method": method,
    //         "args": data,
    //         "kwargs": paramsKwargs,
    //         "context": context
    //     }
    //     ApiService.callJsonRpc(args, function (data) {
    //         success(data)
    //     }, function (data) {
    //         failed(data)
    //     })
    // }
}
