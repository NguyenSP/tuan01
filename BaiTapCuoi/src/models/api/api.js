import axios from 'axios'
import config, { baseURL, db } from '../../utils/config'
import { Platform } from 'react-native';

export default class ApiService {
    static callJsonRpc(args, success, failed, subUrl = "") {
        const params = {
            "params": args
        }

        if (subUrl == "") {
            subUrl = "web/dataset/call_kw"
        }
        // console.warn(params)
        axios.post(baseURL + subUrl, params, {}).then(
            res => {
                if (res.data.result != undefined || (res.data.result == undefined && res.data.error == undefined)) {
             //i       console.warn(data)
                    success(res.data.result)
                }
                else {
              //     console.warn(res.data)
                    failed(res.data.error)
               
                }
            }
        ).catch(err => {
            failed(err)
        }
        )
    }
}