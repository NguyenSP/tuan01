import Model from './model';
import { context } from '../utils/config';

export default class ReeCategory extends Model{
    static name = "";
    static code = "";
    static id =0;

    getObjectName() {
        return "ree.category";
    }

    getCategory(success, fail){
        return this.search_read([],["id", "name", "code"], 0, 10, "", context, success, fail);
    }
}