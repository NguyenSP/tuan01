import {combineReducers} from 'redux';
import ProjectReducer from './ProjectReducer';
import FloorReducer from './FloorReducer'


   const reducer = combineReducers({ // tập hợp các reducer
    ProjectReducer:ProjectReducer, // rudecer 1
    FloorReducer:FloorReducer

});

export default reducer;