import { View, TextInput,Image ,TouchableOpacity,Text,ToastAndroid} from 'react-native';
import axios from 'axios';
import logo from "../assets/images/image_logo.png";
import res_project from '../models/ree_project';
import Res_category from '../models/ree_category';
import Product_product from '../models/product_product';
import res_project_structure from '../models/resee_project_structure';



const DuAnController = {

    _getDataCategory(success,fail) {  

        var res_category = new Res_category();
        res_category.getCategory(success,fail)
    },


    _getDataProject(id_category,success,fail){
        
        var res_list_project = new res_project();
        res_list_project.getObject(id_category,success, fail)
    },

    _getDataDetailProject(id_project,success,fail){
        var res_info_project = new res_project();
        res_info_project.getInfo(id_project,success, fail)
    },

    _getDatalist_id_blockProject(id_project,success,fail){
        var res_info_project = new res_project();
        res_info_project.getlist_id_blockProject(id_project,success, fail)
    },

    _getProduct(list_id_block,success,fail){
        var pr_product = new Product_product();
        pr_product.getProduct(list_id_block,success, fail)
    },

    _getProductForFloor(id_floor,success,fail){
        var pr_product = new Product_product();
        pr_product.getProduct(id_floor,success, fail)
    },

    _getBlockProject(id_project,success,fail){
        var res_block = new res_project_structure();
        res_block.getAllBlock(id_project,success,fail)
    },


    _getParentID(id_block,success,fail){
        var res_block = new res_project_structure();
        res_block.getParentID(id_block,success,fail)
    },

    _extratAllBlock(data){
        var AllBlock =[]
        for(i=0;i<data.length;i++){
          var x = {       id_block:data[i].id,
                          name:data[i].name,
                          display_name:data[i].display_name}
          AllBlock[i]=x
        }   
        return AllBlock; 
    },

    _extratAllFloor(data){
        var AllFloor = []
        for(j=0;j<data.length;j++){
            var x = {    id_block:data[j].parent_id[0],
                         id_floor:data[j].id,
                         name:data[j].name,
                         display_name: data[j].display_name}
              AllFloor[j] =x
            }
        return AllFloor; 
    }
}

export default DuAnController;

