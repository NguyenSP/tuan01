import {StyleSheet} from 'react-native'
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#143974'
    },
    textInput: {
        color :'white',
        height: 40,
        width: 300,
        borderWidth: 2,
        borderColor: '#726D1C',
        borderRadius: 5,
        padding: 10,

    },
    textButton: {
        color: 'white',
        backgroundColor: '#726D1C',
        width: 300,
        height: 40,
        borderRadius: 5,
        padding: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16
    },
    image_logo: {
        width: 200,
        height: 200,
        resizeMode: 'contain', // image scale 
        alignItems:'center',// căng giữa
        alignSelf: 'center',
        },
        logoMain:{
            height: 100,
            marginTop: 50,
            marginBottom: 30,
            alignSelf: 'center',
        },
        splashScreen:{
            width: '100%',
            height: '100%'
        },
        textNameTopicProject: {  
            textAlign: 'center',
            width: '100%',
            color: '#4CAF50',
            fontSize: 20,
            fontWeight: 'bold',
            padding: 8
        },
        

});
