import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    backgroundLogin:{
        backgroundColor:'#143974',
        flex : 1,
    },
    image_logo: {
     width: 200,
     height: 200,
     resizeMode: 'contain', // image scale 
     alignItems:'center',// căng giữa
     },
     text_login:{
         color :'#ffffff',
         borderBottomColor: '#000000',
         borderBottomWidth: 1,
         marginTop:20,
         margin:20,
         marginRight:20,
         marginLeft:20,
     },
     button_login:{
         width:200,
         height:50,
         backgroundColor:'#726D1C',
         alignItems:'center',
         borderColor: '#000000',
         borderWidth: 2,
     },
     text_button_login:{
        flex:1,
        textAlignVertical:'center',
        fontSize:20, 
        color: '#ffffff',
     },
     text_error_login:{
        textAlignVertical:'center',
        fontSize:20,
        fontSize:15,
         color: 'red',
     },
 });
 