import React, {Component} from 'react';
import NavigationConfig from './src/utils/NavigationConfig';
import store from './src/redux/store'
import {Provider} from 'react-redux';

export default class App extends Component{
    render(){
        return (
            <Provider store={store}>
                <NavigationConfig />
            </Provider>
                
          );
    }
}