import React, { Component } from 'react';
import { View, Text,TextInput,FlatList,TouchableOpacity,StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {fetchData,toggleIsAdding} from '../redux/actionCreators';
import styles from '../themes/css';
class Form extends Component{
   constructor(props){
       super(props);
       this.state={
           en:'', vn:'', };
   }

   onAdd(){
       const {en,vn} = this.state;
       this.props.fetchData(en,vn);
       this.props.toggleIsAdding();
   }

    render(){
        const { textInputForm,containerForm} = styles;
        return(
          <View style={containerForm}>
              <TextInput style ={textInputForm}
              value ={this.state.en}
              placeholder='English Word'
              onChangeText={Text=>this.setState({en:Text})}>

              </TextInput>
              <TextInput style ={textInputForm}
              value ={this.state.vn}
              placeholder='Meaning'
              onChangeText={Text=>this.setState({vn:Text})}>

              </TextInput>
              <TouchableOpacity onPress={this.onAdd.bind(this)}>
                  <Text>Add</Text>
              </TouchableOpacity>
          </View>
        );
    }
}
export default connect(null,{fetchData,toggleIsAdding})(Form);