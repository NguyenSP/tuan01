import React, { Component } from 'react';
import { View, Text,FlatList,TouchableOpacity,StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {filterShowAll,memorized,needPractice} from '../redux/actionCreators';
import styles from '../themes/css';
import Controller from '../controllers/Controller';
 class Filter extends Component {

    render(){
        const {myFilterStatus} = this.props;
        return(
            <View style={styles.containerFilter} >
                <TouchableOpacity onPress={()=>this.props.filterShowAll()}>
                    <Text style ={Controller._filterGetTextStyle('SHOW_ALL',myFilterStatus)}>Show All</Text>
                </TouchableOpacity >
                <TouchableOpacity onPress={()=>this.props.memorized()}>
                    <Text style ={Controller._filterGetTextStyle('MEMORIZED',myFilterStatus)}>Memorized</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.props.needPractice()}>
                    <Text style ={Controller._filterGetTextStyle('NEED_PRACTICE',myFilterStatus)}>Need Practice</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

function mapStateToProps(state){
    return{myFilterStatus : state.filterStatus};
}

export default connect(mapStateToProps,{filterShowAll,needPractice,memorized})(Filter);