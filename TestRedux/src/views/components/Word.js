import React, { Component } from 'react';
import {connect} from 'react-redux';
import {showWord,memorizeWord } from '../../redux/actionCreators';
import { View, Text,TouchableOpacity} from 'react-native';
import styles from '../../themes/css';
 class Word extends Component {
      
    render(){
        const {en,vn,id, memorized,isShow} = this.props.item;
        const { containerWord,controllerWord, buttonWord} = styles;
        const textDecorationLine = memorized? 'line-through':'non';
        const memorizedButtonText = memorized? 'forget':'memorized';
        const meaning = isShow ? vn:'------';
        return(
          <View style ={containerWord} >
             <Text style={{textDecorationLine}} >{en}</Text>
             <Text>{meaning}</Text>
             <View style={controllerWord}>
                 <TouchableOpacity style={buttonWord} onPress ={()=>  this.props.memorizeWord(id)}>
                     <Text>{memorizedButtonText}</Text>
                 </TouchableOpacity>
                 <TouchableOpacity style={buttonWord} onPress={()=> this.props.showWord(id)}>
                     <Text>show</Text>
                 </TouchableOpacity>
             </View>
          </View>
        );
    }
}

export default connect(null,{showWord,memorizeWord})(Word);