import React, { Component } from 'react';
import { View, Text,FlatList,TouchableOpacity,StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import Word from '../views/components/Word';
import Filter from '../views/Filter';
import Header from '../views/Header';
import Form from '../views/Form';
import Controller from '../controllers/Controller';
import {toggleIsAdding} from '../redux/actionCreators'; 
 class Main extends Component {

    _Word = ({ item}) => {
        return (<Word item={item} />  )
    }

    render(){
        const {myFilter,myWords} = this.props;
        console.warn("main")
        console.warn(myWords)
        this.props.toggleIsAdding()
        return(
          <View style={{backgroundColor:'yellow',flex:1,alignSelf:'stretch'}}>

           <Header/>

         
          </View>
        );
    }
}


function mapStateToProps(state){
    return{myFilter : state.filterStatus,
           myWords:state.arrWords,
        myIsAdding:state.isAdding};
}

export default connect(mapStateToProps,{toggleIsAdding})(Main);