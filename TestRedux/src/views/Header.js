import React, { Component } from 'react';
import { View, Text,FlatList,TouchableOpacity,StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {toggleIsAdding} from '../redux/actionCreators'; 
import styles from '../themes/css'
 class Header extends Component {

    render(){
        const {myFilter,myWords} = this.props;
        console.warn("header")
        console.warn(myWords)
        return( 
            <View style={styles.header}>
             {/* <Text></Text>
             <Text>My Words</Text>
             <TouchableOpacity onPress ={()=>this.props.toggleIsAdding()}>
             <Text>+</Text>
             </TouchableOpacity> */}
            </View>
        );
    }
}

function mapStateToProps(state){
    return{myFilter : state.filterStatus,
           myWords:state.arrWords,
           myIsAdding:state.isAdding};
}

export default connect(mapStateToProps,{toggleIsAdding})(Header);
