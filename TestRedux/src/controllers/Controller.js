
const Controller = {
  _mainGetWordList(myFilter,myWords){
    if(myFilter==='MEMORIZED') return myWords.filter(e=>e.memorized);
    if(myFilter==='NEED_PRATICE') return myWords.filter(e=>!e.memorized)
    return myWords;
  },
  _filterGetTextStyle(StatusName,myFilterStatus){
    if(StatusName ==myFilterStatus) return {color:'yellow',fontWeight:'bold'}
    return {color:'white',fontWeight:'bold'};
  },
}

export default Controller;

