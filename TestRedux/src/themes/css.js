import { View, Text,FlatList,StyleSheet,TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
    containerWord :{
        backgroundColor:'#D2DEF6',
        padding :10,
        margin:10
    },
    controllerWord:{
        flexDirection:'row',
        justifyContent:'space-around'
    },
    buttonWord:{
        backgroundColor:'white',
        padding:10,
        margin :10
    },
    header:{
        flex:1,
        backgroundColor:'green',
        alignItems:'center',
        justifyContent:'space-between',
        flexDirection:'row',
        paddingHorizontal:20
    } ,
    textInputForm:{
        height :40,
        width :300,
        backgroundColor:'red',
        margin:10,
        paddingHorizontal:10
    },
    containerForm:{
        alignSelf:'stretch',
        justifyContent:'center',
        alignItems:'center'
    },
    containerFilter:{
        backgroundColor:'green',
        flex : 1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
});
export default styles;