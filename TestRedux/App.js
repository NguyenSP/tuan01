import React, { Component } from 'react';
import store from './src/redux/store'
import {Provider} from 'react-redux';
import Main from './src/views/Main';
export default class App extends Component {
    render(){
        return(
            <Provider store={store}> 
                <Main></Main>
            </Provider>
        )
    }
}