import {StyleSheet} from 'react-native'
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#143974'
    },
    textInput: {
        color :'white',
        height: 40,
        width: 300,
        borderWidth: 2,
        borderColor: '#726D1C',
        borderRadius: 5,
        padding: 10,

    },
    textButton: {
        color: 'white',
        backgroundColor: '#726D1C',
        width: 300,
        height: 40,
        borderRadius: 5,
        padding: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16
    }
});
