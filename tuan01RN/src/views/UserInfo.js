import React, { Component } from 'react'
import { View, Text, TouchableWithoutFeedback, FlatList } from 'react-native'
import styles from "../theme/styles"
import CellCustomer from "../views/components/CellCutomer"
export default class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customers: []
        };
    }
    _getMyCustomer() {
         const { user } = this.props.navigation.state.params
         const self = this
        user.getMyCustomer(
            function (data) {
                self.setState({
                    customers: data
                })
            },
            function (data) {

            }
        )
    }
    _renderItem = ({ item, index }) => {
        return (
            <CellCustomer item={item} index={index} />
        
        )
    }
    render() {
        const  user  = this.props.navigation.state.params.user
         const { customers } = this.state
        return (
            <View style={{ flex: 1, justifyContent: "center" }}>
                <Text style={{ textAlign: "center" }}>{user.uid}</Text>
                <Text style={{ textAlign: "center" }}>{user.name}</Text>
                <Text style={{ textAlign: "center" }}>{user.email}</Text>
                <Text style={{ textAlign: "center" }}>{user.session_id}</Text>

                <TouchableWithoutFeedback
                    onPress={() => this._getMyCustomer()}>
                    <Text style={styles.textButton}>Get My Customer</Text>
                </TouchableWithoutFeedback>
                {customers.length > 0 ?
                    <FlatList
                        extraData={this.state.customers}
                        data={this.state.customers}
                        renderItem={this._renderItem}
                    />
                    :
                    null
                }
                    
            </View>
        )
    }
}