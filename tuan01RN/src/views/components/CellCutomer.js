import React, { Component } from 'react'
import { View, Text} from 'react-native'
export default class CellCustomer extends Component {
    render(){
        const {item,index} = this.props
        return(
            <View>
                <Text>------------------   {index + 1}  -------------------------</Text>
                <Text>Name: {item.name}</Text><Text>Email: {item.email}</Text>
            </View>
        )
    }
}