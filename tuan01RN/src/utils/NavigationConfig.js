import { createStackNavigator, createAppContainer } from 'react-navigation';
import Login from '../views/Login'
import UserInfo from '../views/UserInfo'
const navConfig = createStackNavigator({
    LoginScreen: { screen: Login },
    UserInfoScreen: { screen: UserInfo }
})
const AppContainer = createAppContainer(navConfig)

export default AppContainer