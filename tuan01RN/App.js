
import React, { Component } from 'react';
import NavigationConfig from './src/utils/NavigationConfig'


export default class App extends Component {
  render() {
    return (
      <NavigationConfig />
    );
  }
}
