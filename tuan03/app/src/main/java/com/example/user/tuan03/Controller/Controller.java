package com.example.user.tuan03.Controller;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.user.tuan03.Model.ModelOject.UserLogin;
import com.example.user.tuan03.Model.RealtimeData.NhaOXaHoi;
import com.example.user.tuan03.Model.SQlite.SQLiteDatabaseHandler;

import java.util.List;

public class Controller {
    private Context context;
    public Controller(Context context) {
        this.context = context;
    }

    public UserLogin OnclickLogin(String username , String password){
        if(CheckNullText(username,password)==false) {
            Toast.makeText(context, "Vui lòng điền đủ thông tin ", Toast.LENGTH_LONG).show();
            return null;
        }
        else {
            return UserLogin.doLogin(username,password);
        }
    }


    private boolean CheckNullText(String username , String password){
        if(username.equals("") || password.equals(""))
            return false;
        else return true;
    }

    public List<String> getData(){
        SQLiteDatabaseHandler sqLiteDatabaseHandler = new SQLiteDatabaseHandler(context,null,null,1);
        return sqLiteDatabaseHandler.getUser();
    }

    public void AddUser(UserLogin UserLogin){
        SQLiteDatabaseHandler sqLiteDatabaseHandler = new SQLiteDatabaseHandler(context,null,null,1);
        sqLiteDatabaseHandler.Create();
           if(sqLiteDatabaseHandler.CheckUpdate("907")==true)
               sqLiteDatabaseHandler.Update(UserLogin);
           else
              sqLiteDatabaseHandler.addUser(UserLogin);
    }

    public void deleteDataUser(String uid){
        SQLiteDatabaseHandler sqLiteDatabaseHandler = new SQLiteDatabaseHandler(context,null,null,1);
        sqLiteDatabaseHandler.delete(uid);
    }

    public void getDataRealtime(String  duAn, String maDuAn){
        if(duAn.equals("NhaOXaHoi"))
            NhaOXaHoi.NhanData(maDuAn);
    }

    public void addDataRealtime(String duAn,String maDuAn){
        if(duAn.equals("NhaOXaHoi")) {
            NhaOXaHoi.ThemMoi(duAn,new NhaOXaHoi(maDuAn,"Nhà ở xã hội","Khu biệt thự cao cấp Cocoland",
                    "Đường NF4......Bình Dương","2,1 tỷ","72m2","abc"));
        }
    }

    public void checkChuyenKho(String maduAn , String maPhieu,String maKhoNguon,String maKhoDich){

    }
}
