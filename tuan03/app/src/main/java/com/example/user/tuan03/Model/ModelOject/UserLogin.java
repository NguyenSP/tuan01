package com.example.user.tuan03.Model.ModelOject;

import android.util.Log;

import com.example.user.tuan03.network.XPSApiEndpoint;
import com.example.user.tuan03.network.XPSClient;
import com.google.gson.Gson;

import org.json.JSONObject;

import retrofit2.Call;

public class UserLogin extends  User {
    private String db;
    private String session_id;
    private UserContext user_context;


    public UserLogin(String username, String uid, String company_id, String partner_id, String db, String session_id, UserContext user_context) {
        super(username,uid,company_id,partner_id);
        this.db = db;
        this.session_id = session_id;
        this.user_context = user_context;
    }

    public static UserLogin doLogin(String username, String password) {
        XPSApiEndpoint xps = XPSClient.getXPSClient();
        try {
            Call<String> call = xps.authenticate(new LoginParams("hhd_e300_vn_test01", username, password));
            JSONObject objectResponse = new JSONObject(call.execute().body());
            if (objectResponse.has("result")) {
                Gson gson = new Gson();
                UserLogin user = gson.fromJson(objectResponse.getString("result"), UserLogin.class);
                Log.e("test",user.getUid());
                return user;
            } else return null;
        } catch (Exception e) {
            Log.d("error", "Authenticate Error: " + e.toString());
            return null;
        }
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public UserContext getUser_context() {
        return user_context;
    }

    public void setUser_context(UserContext user_context) {
        this.user_context = user_context;
    }

    public static class UserContext {
        private String lang;
        private String tz;
        private String uid;

        public UserContext(String lang, String tz, String uid) {
            this.lang = lang;
            this.tz = tz;
            this.uid = uid;
        }

        public String getLang() {
            return lang;
        }

        public void setLang(String lang) {
            this.lang = lang;
        }

        public String getTz() {
            return tz;
        }

        public void setTz(String tz) {
            this.tz = tz;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }
    }
}


