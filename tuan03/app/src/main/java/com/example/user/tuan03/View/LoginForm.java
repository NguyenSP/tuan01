package com.example.user.tuan03.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.user.tuan03.Controller.Controller;
import com.example.user.tuan03.Model.ModelOject.User;
import com.example.user.tuan03.Model.ModelOject.UserLogin;
import com.example.user.tuan03.Model.RealtimeData.NhaOXaHoi;

import com.example.user.tuan03.R;
import com.example.user.tuan03.network.XPSClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class LoginForm extends AppCompatActivity implements View.OnClickListener {

    private EditText username , password;
    private Button login;
    private ImageView imageView;
    private Controller controllerLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_form);
         Init();
        CheckSessionLogin();
    }

    private void Init(){
        imageView = (ImageView)findViewById(R.id.imagelogo);
        username = (EditText)findViewById(R.id.ed_username);
        username.setText("hhdtest1@hhdgroup.com");
        password =(EditText)findViewById(R.id.ed_password);
        password.setText("123123123");
        login = (Button)findViewById(R.id.bt_login);
        login.setOnClickListener(this);
        controllerLogin = new Controller(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.bt_login){
            new AsyncTask<Void, Void, UserLogin>() {
                @Override
                protected UserLogin doInBackground(Void... voids) {
                    return  controllerLogin.OnclickLogin(username.getText().toString().trim(),password.getText().toString().trim());
                }

                @Override
                protected void onPostExecute(UserLogin UserLogin) {
                    if(UserLogin==null)
                        Toast.makeText(getApplicationContext(), "Vui lòng điền đúng thông tin", Toast.LENGTH_LONG).show();
                    else {
                        controllerLogin.AddUser(UserLogin);
                        NextActivity();
                    }
                }
            }.execute();
        }
    }

    private void CheckSessionLogin(){
        XPSClient xpsClient = new XPSClient();
        if(xpsClient.session_id!=""){
            NextActivity();
        }
    }

    private void NextActivity(){
        Intent intent = new Intent(this,DuAn.class);
        startActivity(intent);
        finish();
    }
}
