package com.example.user.tuan03.Model.RealtimeData;


public class PhieuKho {
    private String maPhieuKho;
    private String loaiphieu;

    public PhieuKho(String maPhieuKho, String loaiphieu) {
        this.maPhieuKho = maPhieuKho;
        this.loaiphieu = loaiphieu;
    }

    public String getMaPhieuKho() {
        return maPhieuKho;
    }

    public void setMaPhieuKho(String maPhieuKho) {
        this.maPhieuKho = maPhieuKho;
    }

    public String getLoaiphieu() {
        return loaiphieu;
    }

    public void setLoaiphieu(String loaiphieu) {
        this.loaiphieu = loaiphieu;
    }
}
