package com.example.user.tuan03.Model.SQlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;


import com.example.user.tuan03.Model.ModelOject.UserLogin;

import java.util.ArrayList;
import java.util.List;

public class SQLiteDatabaseHandler extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "hhdgroup";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "user";

    private static final String KEY_SESSION_ID = "session_id";
    private static final String KEY_UID = "uid";
    private static final String KEY_DB = "db";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_COMPANY_ID = "company_id";
    private static final String KEY_PARTNER_ID = "partner_id";
    private static final String KEY_LANG = "lang";
    private static final String KEY_TZ = "tz";


    public SQLiteDatabaseHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DropTable();
        Create();
    }

    public void Create(){
        SQLiteDatabase db = this.getWritableDatabase();
        String create_user_table = String.format("CREATE TABLE IF NOT EXISTS %s(%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT)",
                TABLE_NAME, KEY_UID, KEY_SESSION_ID, KEY_DB,KEY_USERNAME,KEY_COMPANY_ID,KEY_PARTNER_ID);
        db.execSQL(create_user_table);

        String create_user_context_table = String.format("CREATE TABLE IF NOT EXISTS %s(%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT)",
                "USERCONTEXT", KEY_UID, KEY_LANG, KEY_TZ);
        db.execSQL(create_user_context_table);
    }

    public void DropTable(){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + "user"+ "'");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + "USERCONTEXT"+ "'");
    }

    public Cursor getDatabase(String sql){
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        return  sqLiteDatabase.rawQuery(sql,null);
    }

    public void addUser(UserLogin UserLogin){
        SQLiteDatabase dbSQLite = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_UID,UserLogin.getUid());
        values.put(KEY_SESSION_ID,UserLogin.getSession_id());
        values.put(KEY_DB,UserLogin.getDb());
        values.put(KEY_USERNAME, UserLogin.getUsername());
        values.put(KEY_COMPANY_ID,UserLogin.getCompany_id());
        values.put(KEY_PARTNER_ID, UserLogin.getPartner_id());
        dbSQLite.insert(TABLE_NAME, null, values);

        ContentValues context = new ContentValues();
        context.put(KEY_UID,UserLogin.getUser_context().getUid());
        context.put(KEY_LANG,UserLogin.getUser_context().getLang());
        context.put(KEY_TZ,UserLogin.getUser_context().getTz());
        dbSQLite.insert("USERCONTEXT",null,context);

        dbSQLite.close();

    }

    public List<String> getUser(){
        List<String> list = new ArrayList<String>();
        list.clear();
        Cursor dataTB = getDatabase("SELECT * FROM user");
        while(dataTB.moveToNext()){
            list.add(dataTB.getString(0));
            list.add(dataTB.getString(1));
            list.add(dataTB.getString(2));
            list.add(dataTB.getString(3));
            list.add(dataTB.getString(4));
        }

        Cursor dataTB2 = getDatabase("SELECT * FROM USERCONTEXT");
        while(dataTB2.moveToNext()){
        }
        return list;
    }

    public boolean CheckUpdate(String uid){
        Cursor dataTB = getDatabase("SELECT * FROM user where uid="+uid);
        if(dataTB.getCount()>0){
            return  true;
        }else return false;
    }

    public void Update(UserLogin UserLogin){
        SQLiteDatabase dbSQLite = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_UID,UserLogin.getUid());
        values.put(KEY_SESSION_ID,UserLogin.getSession_id());
        values.put(KEY_DB,UserLogin.getDb());
        values.put(KEY_USERNAME, UserLogin.getUsername());
        values.put(KEY_COMPANY_ID,UserLogin.getCompany_id());
        values.put(KEY_PARTNER_ID,UserLogin.getPartner_id());
        dbSQLite.update(TABLE_NAME,values,"uid=907",null);

        ContentValues context = new ContentValues();
        context.put(KEY_UID,UserLogin.getUser_context().getUid());
        context.put(KEY_LANG,UserLogin.getUser_context().getLang());
        context.put(KEY_TZ,UserLogin.getUser_context().getTz());
        dbSQLite.update("USERCONTEXT",context,"uid=907",null);

        dbSQLite.close();
    }

    public void delete(String uid){
        SQLiteDatabase dbSQLite = this.getWritableDatabase();
        dbSQLite.delete("user","uid=907",null);
        dbSQLite.delete("USERCONTEXT","uid=907",null);
    }


}
