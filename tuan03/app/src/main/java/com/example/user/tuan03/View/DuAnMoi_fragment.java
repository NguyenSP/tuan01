package com.example.user.tuan03.View;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.user.tuan03.Model.RealtimeData.NhaDat;
import com.example.user.tuan03.Model.RealtimeData.NhaOXaHoi;
import com.example.user.tuan03.Model.adapter.DuAnAdapter;
import com.example.user.tuan03.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;


public class DuAnMoi_fragment extends Fragment {
    private RecyclerView recyclerView,thmai,bds;
    private DuAnAdapter mAdapter;
    public static FirebaseDatabase database;
    public static DatabaseReference myDataRefer;
    public static List<NhaDat> list = new ArrayList<NhaDat>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_du_an_moi_fragment, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyview_nha_o_xa_hoi);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        recyclerView.setHasFixedSize(true);

        thmai = (RecyclerView)view.findViewById(R.id.recyview_nha_o_thuong_mai);
        StaggeredGridLayoutManager gridLayoutManager2 =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        thmai.setLayoutManager(gridLayoutManager2);
        thmai.setHasFixedSize(true);

        bds = (RecyclerView)view.findViewById(R.id.recyview_bdscc);
        StaggeredGridLayoutManager gridLayoutManager3 =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        bds.setLayoutManager(gridLayoutManager3);
        bds.setHasFixedSize(true);
        database = FirebaseDatabase.getInstance();
        myDataRefer = database.getReference("NhaDat");
        myDataRefer.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.child("DuAn").child("NhaOXaHoi").getChildren()) {
                    Gson gson = new Gson();
                    String json = gson.toJson(data.getValue());
                    NhaOXaHoi test = gson.fromJson(json, NhaOXaHoi.class);
                    list.add(test);
                }
                if(recyclerView!=null && thmai!=null && bds!=null ) {
                    try {
                        mAdapter = new DuAnAdapter(getActivity(), list);
                        recyclerView.setAdapter(mAdapter);
                        thmai.setAdapter(mAdapter);
                        bds.setAdapter(mAdapter);
                    }catch (Exception e){
                        return;
                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        return view;
    }
}
