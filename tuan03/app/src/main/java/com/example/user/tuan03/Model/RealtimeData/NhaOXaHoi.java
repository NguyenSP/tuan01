package com.example.user.tuan03.Model.RealtimeData;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.example.user.tuan03.R;
import com.example.user.tuan03.View.DuAnMoi_fragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;


public class NhaOXaHoi extends NhaDat {
    public static FirebaseDatabase database;
    public static DatabaseReference myDataRefer;


    public NhaOXaHoi(String maDuAn, String tenDuAn, String tieuDe, String diaChi, String giaTien, String dienTich, String urlImage) {
        super(maDuAn, tenDuAn, tieuDe, diaChi, giaTien, dienTich, urlImage);
    }

    public static void Init() {
        database = FirebaseDatabase.getInstance();
        myDataRefer = database.getReference("NhaDat");
    }

    public static void NhanData(final String maDuAn) {
        Init();
     }
}
