package com.example.user.tuan03.Model.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.tuan03.Model.RealtimeData.ChuyenKho;
import com.example.user.tuan03.Model.RealtimeData.DanhSachKho;
import com.example.user.tuan03.Model.RealtimeData.DuAnKho;
import com.example.user.tuan03.Model.RealtimeData.NhaDat;
import com.example.user.tuan03.Model.RealtimeData.PhieuKho;
import com.example.user.tuan03.R;
import com.example.user.tuan03.View.Kho;
import com.example.user.tuan03.View.KhoChuyenKho;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ChuyenKhoAdapter extends RecyclerView.Adapter<ChuyenKhoAdapter.KhoViewHolder>{
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private static List<ChuyenKho> listChuyenKho = new ArrayList<ChuyenKho>();
    private static List<DuAnKho> listDuAnKho = new ArrayList<DuAnKho>();
    private static List<PhieuKho> listPhieuKho = new ArrayList<PhieuKho>();
    private static List<DanhSachKho>listDanhSachKho = new ArrayList<DanhSachKho>();
    private int vitri;

    public ChuyenKhoAdapter(Context context, List<ChuyenKho> listChuyenKho,List<DuAnKho> listDuAnKho , List<PhieuKho>  listPhieuKho,List<DanhSachKho>listDanhSachKho) {
        this.mContext = context;
        this.listChuyenKho = listChuyenKho;
        this.listDuAnKho = listDuAnKho;
        this.listPhieuKho = listPhieuKho;
        this.listDanhSachKho = listDanhSachKho;
        mLayoutInflater = LayoutInflater.from(context);
        vitri = listChuyenKho.size();
    }


    public class KhoViewHolder extends RecyclerView.ViewHolder{

        public TextView maPhieu,duAn,ngayTao;
        public LinearLayout row;


        public KhoViewHolder(final View itemView) {
            super(itemView);
            maPhieu = (TextView) itemView.findViewById(R.id.txt_kho_ma_phieu);
            duAn = (TextView) itemView.findViewById(R.id.txt_kho_du_an);
            ngayTao = (TextView) itemView.findViewById(R.id.txt_kho_ngay_tao);
            row =(LinearLayout)itemView.findViewById(R.id.row_list_chuyen_kho);
        }
    }


    @Override
    public KhoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = mLayoutInflater.inflate(R.layout.list_chuyen_kho,parent,false);
        return new KhoViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull KhoViewHolder holder, final int position) {
        holder.maPhieu.setText(listChuyenKho.get(position).getLoaiPhieu());
        holder.duAn.setText(listChuyenKho.get(position).getMaDuAn());
        holder.ngayTao.setText(listChuyenKho.get(position).getNgayTao());
        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vitri=position;
                notifyDataSetChanged();

            }
        });
        if(vitri==position) {
            holder.row.setBackgroundColor(Color.parseColor("#7b8d77"));
            Intent intent = new Intent(mContext,KhoChuyenKho.class);
            intent.putExtra("vitri_chuyenkho",position);
            intent.putExtra("vitri_duan",getVitriDuAnKho(position));
            intent.putExtra("vitri_loaiphieu",getVitriLoaiPhieu(position));
            intent.putExtra("vitri_khonguon",getVitriKhoNguon(position));
            intent.putExtra("vitri_khodich",getVitriKhoDich(position));
            mContext.startActivity(intent);
        }
        else
            holder.row.setBackgroundColor(Color.parseColor("#ffffff"));

    }

    private int getVitriDuAnKho(int itemClick){
        int i=-1;
        for(i=0;i<listDuAnKho.size();i++) {
            if (listChuyenKho.get(itemClick).getMaDuAn().equals(listDuAnKho.get(i).getMaDuAnKho()))
                return i;
        }
        return i;
    }

    private int getVitriLoaiPhieu(int itemClick){
        int i=-1;
        for(i=0;i<listPhieuKho.size();i++) {
            if (listChuyenKho.get(itemClick).getLoaiPhieu().equals(listPhieuKho.get(i).getMaPhieuKho()))
                return i;
        }
        return i;
    }
    private int getVitriKhoNguon(int itemClick){
        int i=-1;
        for(i=0;i<listDanhSachKho.size();i++) {
            if (listChuyenKho.get(itemClick).getKhoNguon().equals(listDanhSachKho.get(i).getMaKho()))
                return i;
        }
        return i;
    }

    private int getVitriKhoDich(int itemClick){
        int i=-1;
        for(i=0;i<listDanhSachKho.size();i++) {
            if (listChuyenKho.get(itemClick).getKhoDich().equals(listDanhSachKho.get(i).getMaKho()))
                return i;
        }
        return i;
    }

    @Override
    public int getItemCount() {
        return listChuyenKho.size();
    }
}
