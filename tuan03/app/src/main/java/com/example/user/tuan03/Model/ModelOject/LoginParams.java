package com.example.user.tuan03.Model.ModelOject;

public class LoginParams {
    public LoginInformation params;

    public LoginParams(String db, String login, String password) {
        this.params = new LoginInformation(db, login, password);
    }

    public static class LoginInformation {
        public String db;
        public String login;
        public String password;

        public LoginInformation(String db, String login, String password) {
            this.db = db;
            this.login = login;
            this.password = password;
        }
    }
}