package com.example.user.tuan03.Model.RealtimeData;

public class ChuyenKho  {
    private String loaiPhieu;
    private String khoDich;
    private String khoNguon;
    private String maDuAn;
    private String ngayTao;

    public ChuyenKho(String loaiPhieu, String khoDich, String khoNguon, String maDuAn, String ngayTao) {
        this.loaiPhieu = loaiPhieu;
        this.khoDich = khoDich;
        this.khoNguon = khoNguon;
        this.maDuAn = maDuAn;
        this.ngayTao = ngayTao;
    }

    public String getLoaiPhieu() {
        return loaiPhieu;
    }

    public void setLoaiPhieu(String loaiPhieu) {
        this.loaiPhieu = loaiPhieu;
    }

    public String getKhoDich() {
        return khoDich;
    }

    public void setKhoDich(String khoDich) {
        this.khoDich = khoDich;
    }

    public String getKhoNguon() {
        return khoNguon;
    }

    public void setKhoNguon(String khoNguon) {
        this.khoNguon = khoNguon;
    }

    public String getMaDuAn() {
        return maDuAn;
    }

    public void setMaDuAn(String maDuAn) {
        this.maDuAn = maDuAn;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }
}
