package com.example.user.tuan03.View;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.example.user.tuan03.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class QRActivity extends AppCompatActivity {
    private TextView tvname,tvemail;
    private ImageButton btnScan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        init();
        final IntentIntegrator integrator = new IntentIntegrator(QRActivity.this);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                integrator.initiateScan();
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.e("test",result.getContents());
                try {
                    JSONObject jsonObject=new JSONObject(result.getContents());
                    tvname.setText(jsonObject.getString("name"));
                    tvemail.setText(jsonObject.getString("email"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    private void init() {
        tvname=(TextView)findViewById(R.id.qrTvName);
        tvemail=(TextView)findViewById(R.id.qrTvEmail);
        btnScan=(ImageButton)findViewById(R.id.qrBtnScan);
    }
}
