package com.example.user.tuan03.Model.RealtimeData;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class NhaDat {
    private String maDuAn;
    private String tenDuAn;
    private String tieuDe;
    private String diaChi;
    private String giaTien;
    private String dienTich;
    private String url;
    public static   FirebaseDatabase database;
    public static DatabaseReference myDataRefer;

    public NhaDat(String maDuAn ,String tenDuAn, String tieuDe, String diaChi, String giaTien, String dienTich,String url) {
        this.maDuAn = maDuAn;
        this.tenDuAn = tenDuAn;
        this.tieuDe = tieuDe;
        this.diaChi = diaChi;
        this.giaTien = giaTien;
        this.dienTich = dienTich;
        this.url = url;

    }
    public static void Init(){
        database = FirebaseDatabase.getInstance();
        myDataRefer = database.getReference("NhaDat");
    }
    public static void ThemMoi(String DuAn, NhaDat duAnNhaDat) {
        Init();
        if(DuAn.equals("NhaOXaHoi"))
            myDataRefer.child("DuAn").child("NhaOXaHoi").child(duAnNhaDat.getMaDuAn()).setValue(duAnNhaDat);
        else if(DuAn.equals("NhaOXaHoi"))
            myDataRefer.child("DuAn").child("NhaOThuongMai").child(duAnNhaDat.getMaDuAn()).setValue(duAnNhaDat);
        else if(DuAn.equals("BatDongSanCaoCap"))
            myDataRefer.child("DuAn").child("BatDongSanCaoCap").child(duAnNhaDat.getMaDuAn()).setValue(duAnNhaDat);
    }

    public static void XoaData(String DuAn,String maDuAn){
        Init();
        if(DuAn.equals("NhaOXaHoi"))
            myDataRefer.child("DuAn").child("NhaOXaHoi").child(maDuAn).setValue(new NhaOXaHoi(null,null,null,null,null,null,null));
        else if(DuAn.equals("NhaOXaHoi"))
            myDataRefer.child("DuAn").child("NhaOThuongMai").child(maDuAn).setValue(new NhaOXaHoi(null,null,null,null,null,null,null));
        else if(DuAn.equals("BatDongSanCaoCap"))
            myDataRefer.child("DuAn").child("BatDongSanCaoCap").child(maDuAn).setValue(new NhaOXaHoi(null,null,null,null,null,null,null));
    }

    public String getTenDuAn() {
        return tenDuAn;
    }

    public void setTenDuAn(String tenDuAn) {
        this.tenDuAn = tenDuAn;
    }

    public String getTieuDe() {
        return tieuDe;
    }

    public void setTieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(String giaTien) {
        this.giaTien = giaTien;
    }

    public String getDienTich() {
        return dienTich;
    }

    public void setDienTich(String dienTich) {
        this.dienTich = dienTich;
    }

    public String getMaDuAn() { return maDuAn; }

    public void setMaDuAn(String maDuAn) { this.maDuAn = maDuAn; }

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }
}
