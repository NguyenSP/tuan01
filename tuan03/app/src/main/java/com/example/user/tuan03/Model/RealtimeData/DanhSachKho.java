package com.example.user.tuan03.Model.RealtimeData;

public class DanhSachKho {
    private String maKho;
    private String tenKho;

    public DanhSachKho(String maKho, String tenKho) {
        this.maKho = maKho;
        this.tenKho = tenKho;
    }

    public String getMaKho() {
        return maKho;
    }

    public void setMaKho(String maKho) {
        this.maKho = maKho;
    }

    public String getTenKho() {
        return tenKho;
    }

    public void setTenKho(String tenKho) {
        this.tenKho = tenKho;
    }


}
