package com.example.user.tuan03.View;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;


import com.example.user.tuan03.Controller.Controller;
import com.example.user.tuan03.Model.RealtimeData.ChuyenKho;
import com.example.user.tuan03.Model.RealtimeData.DanhSachKho;
import com.example.user.tuan03.Model.RealtimeData.DuAnKho;
import com.example.user.tuan03.Model.RealtimeData.PhieuKho;
import com.example.user.tuan03.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class KhoChuyenKho extends AppCompatActivity implements View.OnClickListener {
    private ImageView bt_back;
    private Toolbar toolbar;
    private Button bt_xoa,bt_tao,bt_scan;
    private Spinner spin_du_an,spin_loai_phieu,spin_kho_nguon,spin_kho_dich;
    public static FirebaseDatabase database;
    public static DatabaseReference myDataRefer;
    private static List<DuAnKho> listDuAnKho = new ArrayList<DuAnKho>();
    private static List<PhieuKho> listPhieuKho = new ArrayList<PhieuKho>();
    private static List<DanhSachKho>listDanhSachKho = new ArrayList<DanhSachKho>();
    private static List<ChuyenKho> listChuyenKho = new ArrayList<ChuyenKho>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kho_chuyen_kho);
        getData();
        Init();
    }

    private void Init(){
        toolbar =(Toolbar)findViewById(R.id.toolbar_kho);
        bt_back =(ImageView)findViewById(R.id.bt_back_kho_chuyen);
        bt_back.setOnClickListener(this);
        bt_tao =(Button)findViewById(R.id.kho_bt_tao_phieu);
        bt_tao.setOnClickListener(this);
        bt_xoa =(Button)findViewById(R.id.kho_bt_xoa);
        bt_xoa.setOnClickListener(this);
        bt_scan =(Button)findViewById(R.id.bt_scan) ;
        bt_scan.setOnClickListener(this);
        spin_du_an=(Spinner) findViewById(R.id.sp_du_an);
        spin_loai_phieu=(Spinner) findViewById(R.id.sp_loai_phieu);
        spin_kho_nguon=(Spinner) findViewById(R.id.sp_kho_nguon);
        spin_kho_dich=(Spinner) findViewById(R.id.sp_kho_dich);
    }

    private SpinnerAdapter setAdapter(List<String> list){
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        return  adapter;
    }

    private void getData(){
        database = FirebaseDatabase.getInstance();
        myDataRefer = database.getReference("NhaDat");
        myDataRefer.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listDuAnKho.clear();
                listDanhSachKho.clear();
                listPhieuKho.clear();
                listChuyenKho.clear();
                List<String> stringlistDuAnKho = new ArrayList<String>();
                stringlistDuAnKho.add("Chọn dự án:");
                List<String> stringlistDanhSachKho = new ArrayList<String>();
                stringlistDanhSachKho.add("chọn kho:");
                List<String> stringlistPhieuKho = new ArrayList<String>();
                stringlistPhieuKho.add("chọn loại phiếu:");

                for (DataSnapshot data : dataSnapshot.child("Kho").child("ChuyenKho").getChildren()) {
                    Gson gson = new Gson();
                    String json = gson.toJson(data.getValue());
                    ChuyenKho chuyenkho = gson.fromJson(json, ChuyenKho.class);
                    listChuyenKho.add(chuyenkho);
                }
                for (DataSnapshot data : dataSnapshot.child("Kho").child("DSKho").getChildren()) {
                    listDanhSachKho.add(new DanhSachKho(data.getKey().toString(), data.getValue().toString()));
                    stringlistDanhSachKho.add(data.getValue().toString());

                }
                for (DataSnapshot data : dataSnapshot.child("Kho").child("DuAn").getChildren()) {
                    listDuAnKho.add(new DuAnKho(data.getKey().toString(), data.getValue().toString()));
                    stringlistDuAnKho.add(data.getValue().toString());
                }
                for (DataSnapshot data : dataSnapshot.child("Kho").child("PhieuKho").getChildren()) {
                    listPhieuKho.add(new PhieuKho(data.getKey().toString(), data.getValue().toString()));
                    stringlistPhieuKho.add(data.getValue().toString());
                }
                Intent intent = getIntent();
                spin_du_an.setAdapter(setAdapter(stringlistDuAnKho));
                spin_du_an.setSelection(intent.getIntExtra("vitri_duan",0)+1);
                spin_loai_phieu.setAdapter(setAdapter(stringlistPhieuKho));
                spin_loai_phieu.setSelection(intent.getIntExtra("vitri_loaiphieu",0)+1);
                spin_kho_nguon.setAdapter(setAdapter(stringlistDanhSachKho));
                spin_kho_nguon.setSelection(intent.getIntExtra("vitri_khonguon",0)+1);
                spin_kho_dich.setAdapter(setAdapter(stringlistDanhSachKho));
                spin_kho_dich.setSelection(intent.getIntExtra("vitri_khodich",0)+1);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.bt_back_kho_chuyen){
            Intent intent = new Intent(this, Kho.class);
            startActivity(intent);
            finish();
        }
        else if (v.getId()==R.id.kho_bt_tao_phieu){
            Date presentTime_Date = Calendar.getInstance().getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String s = dateFormat.format(presentTime_Date);
            ChuyenKho chuyenKho = new ChuyenKho(   listPhieuKho.get(spin_loai_phieu.getSelectedItemPosition()-1).getMaPhieuKho(),
                                                    listDanhSachKho.get(spin_kho_dich.getSelectedItemPosition()-1).getMaKho(),
                                                    listDanhSachKho.get(spin_kho_nguon.getSelectedItemPosition()-1).getMaKho(),
                                                     listDuAnKho.get(spin_du_an.getSelectedItemPosition()-1).getMaDuAnKho(),s
                                                    );
            myDataRefer.child("Kho").child("ChuyenKho").child("MaPhieu"+(listChuyenKho.size()+1)).setValue(chuyenKho);
            Intent intent = new Intent(this,Kho.class);
            startActivity(intent);
            finish();

        }
        else  if(v.getId()==R.id.bt_scan){
            Intent intent = new Intent(this,QRActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
