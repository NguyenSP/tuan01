package com.example.user.tuan03.View;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.user.tuan03.Model.RealtimeData.ChuyenKho;
import com.example.user.tuan03.Model.RealtimeData.DanhSachKho;
import com.example.user.tuan03.Model.RealtimeData.DuAnKho;
import com.example.user.tuan03.Model.RealtimeData.PhieuKho;
import com.example.user.tuan03.Model.adapter.ChuyenKhoAdapter;
import com.example.user.tuan03.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Kho extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private ChuyenKhoAdapter chuyenKhoAdapter;
    public static FirebaseDatabase database;
    public static DatabaseReference myDataRefer;
    private static List<ChuyenKho> listChuyenKho = new ArrayList<ChuyenKho>();
    private static List<DuAnKho> listDuAnKho = new ArrayList<DuAnKho>();
    private static List<PhieuKho> listPhieuKho = new ArrayList<PhieuKho>();
    private static List<DanhSachKho>listDanhSachKho = new ArrayList<DanhSachKho>();
    private TabLayout tabLayout;
    private ImageView bt_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kho);
        recyclerView = (RecyclerView)findViewById(R.id.recyview_chuyen_kho);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));
        recyclerView.setHasFixedSize(true);
        bt_back =(ImageView)findViewById(R.id.bt_back_kho);
        bt_back.setOnClickListener(this);
        getData();

    }

    private void getData(){
        database = FirebaseDatabase.getInstance();
        myDataRefer = database.getReference("NhaDat");
        myDataRefer.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listChuyenKho.clear();
                listDuAnKho.clear();
                listDanhSachKho.clear();
                listPhieuKho.clear();
                for (DataSnapshot data : dataSnapshot.child("Kho").child("ChuyenKho").getChildren()) {
                    Gson gson = new Gson();
                    String json = gson.toJson(data.getValue());
                    ChuyenKho chuyenkho = gson.fromJson(json, ChuyenKho.class);
                    listChuyenKho.add(chuyenkho);
                }
                for (DataSnapshot data : dataSnapshot.child("Kho").child("DSKho").getChildren()) {
                    listDanhSachKho.add(new DanhSachKho(data.getKey().toString(), data.getValue().toString()));
                }
                for (DataSnapshot data : dataSnapshot.child("Kho").child("DuAn").getChildren()) {
                    listDuAnKho.add(new DuAnKho(data.getKey().toString(), data.getValue().toString()));
                }
                for (DataSnapshot data : dataSnapshot.child("Kho").child("PhieuKho").getChildren()) {
                    listPhieuKho.add(new PhieuKho(data.getKey().toString(), data.getValue().toString()));
                }
                chuyenKhoAdapter = new ChuyenKhoAdapter(getApplicationContext(),listChuyenKho,listDuAnKho,listPhieuKho,listDanhSachKho);
                recyclerView.setAdapter(chuyenKhoAdapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        tabLayout = (TabLayout) findViewById(R.id.tabLayout_kho_bottom);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==2){
                    Intent intent = new Intent(getApplicationContext(),KhoChuyenKho.class);
                    intent.putExtra("vitri_chuyenkho",-1);
                    intent.putExtra("vitri_duan",-1);
                    intent.putExtra("vitri_loaiphieu",-1);
                    intent.putExtra("vitri_khonguon",-1);
                    intent.putExtra("vitri_khodich",-1);
                    startActivity(intent);
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.bt_back_kho){
            Intent intent = new Intent(this,DuAn.class);
            startActivity(intent);
            finish();
        }
    }
}
