package com.example.user.tuan03.Model.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.user.tuan03.Model.RealtimeData.NhaDat;
import com.example.user.tuan03.Model.RealtimeData.NhaOXaHoi;
import com.example.user.tuan03.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DuAnAdapter extends RecyclerView.Adapter<DuAnAdapter.MyViewHolder>{
    private List<NhaDat> nhaDatList;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private static String flag="";

    public DuAnAdapter(Context context, List<NhaDat> nhaDatList) {
        this.nhaDatList = nhaDatList;
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tieu_de_1,dia_chi_1,gia_tien_1;
        private String urlImage;
        private ImageView image;

        public MyViewHolder(final View itemView) {
            super(itemView);
            tieu_de_1 = (TextView) itemView.findViewById(R.id.text_tieu_de_1);
            dia_chi_1 = (TextView) itemView.findViewById(R.id.text_dia_chi_1);
            gia_tien_1 = (TextView) itemView.findViewById(R.id.text_gia_tien_1);
            image = (ImageView)itemView.findViewById(R.id.img_can_ho_1);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = mLayoutInflater.inflate(R.layout.list_du_an,parent,false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(DuAnAdapter.MyViewHolder holder, int position) {
        NhaDat nhaDat = nhaDatList.get(position);
            holder.tieu_de_1.setText(nhaDat.getTieuDe());
            holder.dia_chi_1.setText(nhaDat.getDiaChi());
            holder.gia_tien_1.setText(nhaDat.getGiaTien());
        String styledText = "Từ:<br> <font color='red'>"+nhaDat.getGiaTien()+"</font>/"+nhaDat.getDienTich();
        holder.gia_tien_1.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
        Picasso.with(mContext)
                .load(nhaDat.getUrl())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher_round)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return nhaDatList.size();
    }
}
