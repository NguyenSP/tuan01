package com.example.user.tuan03.Model.RealtimeData;

public class DuAnKho {
    private String maDuAnKho;
    private String tenDuAnKho;

    public DuAnKho(String maDuAnKho, String tenDuAnKho) {
        this.maDuAnKho = maDuAnKho;
        this.tenDuAnKho = tenDuAnKho;
    }

    public String getMaDuAnKho() {
        return maDuAnKho;
    }

    public void setMaDuAnKho(String maDuAnKho) {
        this.maDuAnKho = maDuAnKho;
    }

    public String getTenDuAnKho() {
        return tenDuAnKho;
    }

    public void setTenDuAnKho(String tenDuAnKho) {
        this.tenDuAnKho = tenDuAnKho;
    }
}
