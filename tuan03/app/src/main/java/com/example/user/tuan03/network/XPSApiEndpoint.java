package com.example.user.tuan03.network;

import com.example.user.tuan03.Model.ModelOject.LoginParams;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface XPSApiEndpoint {

    @POST("/web/session/authenticate")
    Call<String> authenticate(
            @Body LoginParams param);

}
